"""
Lookuptable for the triangular cell in the 488 color code

@author: pedro parrado
"""

# import numpy as np
import time

start = time.time()

# the lookuptable is a list of sets, one per syndrome
# each set has the combinations of errors that trigger it
# for example, 100100101000 would mean errors in the
# qubits 0,3, 6 and 8


lut = []
for i in range(2 ** 12):
    lut.append(set())

'''
Notation for qubits starting from the bottom left, increasing to the right by rows.
Notation for the stabilizers starting bottom left, counter-clockwise,
first the splittings and second the bulk stabilizers
 |\13|14/|\17|              5    4       
 |12\|/15|16\|          |\   | / |\ |
 |6 /|\9 |10/|         6| \11|/10| \| 3
 |/7 | 8\|/11|          | /  |\  | /|
 |\1 |2 /|\ 5|         7|/ 8 | \9|/ | 2
 |0 \|/ 3|4 \|          |\   | / |\ |
                        | \  |/  | \|
                            0    1

Notation from stabilizers starting from the bottom left (shared by qubits 0-3) 
and following counter-clockwise. Stabilizer 6 corresponds to the 4-qubit stab inside,
shared by 1,2,6,7 
'''

# stabilizers correspondent to each qubit
stabsInQubit = [
[0,7],      # 0 (bottom-left corner)
[0,7,8],    # 1
[0,8,9],    # 2
[0,1,9],    # 3
[1,9],    # 4
[2,9],    # 5
[6,7,11],    # 6
[7,8,11],    # 7
[8,9,11],    # 8
[9,10,11],    # 9
[3,9,10],    # 10
[2,3,9],    # 11
[6,11],    # 12
[5,11],    # 13
[4,5,11],    # 14
[4,10,11],    # 15
[3,4,10],    # 16
[3,4]    # 17
]
# qubits correspondent to each stabilizer
stabs = [
[0,1,2,3],                  # 0
[3,4],                      # 1
[5,11],                     # 2
[11,10,16,17],              # 3
[14,15,16,17],              # 4
[13,14],                    # 5
[6,12],                     # 6
[0,1,6,7],                  # 7
[1,2,7,8],                  # 8
[2,3,4,5,8,9,10,11],        # 9
[9,10,15,16],               # 10
[6,7,8,9,12,13,14,15]       # 11
]


'''
Now, we will define the logical operators and bulk stabilizers.
For each syndrome, given a valid correction, 
we can generate the rest of possible corrections compatible with
that syndrome by adding a combination of logical operators and 
bulk stabilizers.

In the following lines, we will define this set of possible corrections
for the syndrome ++++++

'''

# logical operators
L0 = set([0, 3, 4, 6,12])
L1 = set([5,11,13,14,17])

# bulk stabilizers
bulkStabs = [set([1,2,7,8]),                #8
             set([2,3,4,5,8,9,10,11]),      #9
             set([9,10,15,16]),             #10
             set([6,7,8,9,12,13,14,15])]    #11


# bulk stabilizer permutations

bulkStabPermutations = []
for i in range(2):
    for j in range(2):
        for k in range(2):
            for l in range(2):

                per = set([])
                if i>0:
                    per = bulkStabs[0]^per
                if j>0:
                    per = bulkStabs[1]^per
                if k>0:
                    per = bulkStabs[2]^per
                if l>0:
                    per = bulkStabs[3]^per
                bulkStabPermutations.append(per)

# logic operator permutations
logicOpPermutations = [
    set([]),
    L0,
    L1,
    L0^L1
]

'''
Now, we will store in generators a list with all the possible sets of 
equivalent corrections, as a list of sets containing the qubits acted on

e.g.

generators = [set(),
 {6, 7, 8, 9, 12, 13, 14, 15},
 {9, 10, 15, 16},
 {6, 7, 8, 10, 12, 13, 14, 16},
 {2, 3, 4, 5, 8, 9, 10, 11},
 ...
 ]
 
 It is important to note that the possible corrections in 
 the generators list will be ordered in the following manner:
 
 first,  all 2^4 combinations equivalent to NO logical operator applied
 second, all 2^4 combinations equivalent to L0 being applied
 third,  all 2^4 combinations equivalent to L1 being applied
 last ,  all 2^4 combinations equivalent to L0 and L1 being applied
 
L0 is the logical operator of the lower left cell
L1 is the logical operator of the top right cell

'''
generators = []


for logicOp in logicOpPermutations:
    for stabPermutation in bulkStabPermutations:
        generators.append(logicOp ^ stabPermutation)



'''
Here, we define some functions to apply these operators in 
set format (e.g. op = [1,5,6,9])
to a state in string format (e.g. state = "001001010000010").

We also define a function with which we can generate all possible
valid corrections for a syndrome. It requires an initial valid 
correction for that syndrome, and using it, the function
genstates(state) will apply all possible combinations of the 
logical ops and stabilizers, as defined in the list "generators"
'''


def applyop(op, state):
    '''
    Toggles qubits in state "state" when applying an operator "op".
    the operator op is a list of qubits affected,
    state is a string of 01101010, which is
    going to be modified
    '''
    state2 = ''
    for i in range(len(state)):
        newq = int(state[i])
        if i in set(op):
            newq = (newq + 1) % 2
        state2 += str(newq)
    return state2


def genstates(correction):
    '''
    Given a valid correction for a syndrome, generates the full list of possible
    corrections for that syndrome. To do that, it applies all possible permutations
    of logical operators and bulk stabilizers.
    :param correction: a valid correction in string format (e.g. "001001010010")
    :return: a list with all possible corrections (e.g. ["01010","101001",...] )
    '''

    s = list()
    for gen in generators:
        s.append(applyop(gen, correction))
    return s


'''
Now, we will fill the lookuptable by finding a solution for each syndrome, 
and then applying genstates to it (which will apply all permutations of logic
operators and bulk stabilizers)

To find the solutions, we will go first through all possible error combinations
and generate an initial disordered lookuptable lut
'''

# initialization of the first unordered lookup table as a list of sets
lut = []
nstabs = 12
for i in range(2 ** nstabs):
    lut.append(set())

nqubits = 18
for i in range(2 ** nqubits):
    # these two lines generate all possible binary combinations
    er = bin(i)[2:]
    er = '0' * (nqubits - len(er)) + er

    syn = [0] * nstabs

    # now, we check the syndrome by going through each qubit and
    # changing the parity of the affected stabilizers
    for q in range(nqubits):
        if er[q] == '1':
            for s in stabsInQubit[q]:
                syn[s] = (syn[s] + 1) % 2

    # now we find the index of this syndrome using its translation to binary
    synb = ''
    for j in syn:
        synb += str(j)
    index = int(synb, 2)

    # and add this error configuration to the lookuptable
    lut[index].add(er)

'''
# now, we will go thorough all elements in the lookuptable, and we will
# reorder them so that they apply logical ops in order 
(L0,L1) => (0,0),(1,0),(0,1),(1,1)

we will use the error configuration with the minimum amount of errors as 
the reference, to then add to it all possible stab and logicOp permutations
with the function genstates

The ordered lookuptable lutordered is now a list of lists
'''
def sumc(c):
    # number of errors in a configuration
    s = 0
    for e in c:
        s += int(e)
    return s

# ordered lookuptable
lutordered = []
for unordered in lut:
    # unordered corresponds to the set of configurations in the lookuptable
    # for a given syndrome

    # we will find the one with the least amount of errors
    mine = 900 # initial minimum or errors, way greater than nqubits
    minc = []  # configuration with the minimum amount of errors

    for conf in unordered:
        nerrors = sumc(conf)
        '''
        if nerrors==mine:
            print("2 error configurations with the same amount of errors")
            print(conf)
            print(minc)
        '''
        if nerrors < mine:
            mine = sumc(conf)
            minc = conf
    # once we found the configuration with the minimum amount of errors,
    # we generate all the configurations again in order
    lutordered.append(genstates(minc))


# last thing to do, store the lookuptable

import numpy as np

np.save("lookuptable18qcell", lutordered)

def formatTime(start):
    t = time.time()-start
    if t<60:
        return str(round(t,2))+ " s"
    elif t<3600:
        min = t//60
        s = t-min*60
        return(str(int(min)) + " min "+str(int(s))+" s")
    else:
        h = t//3600
        min = (t-h*3600)//60
        s = t-min*60 - h*3600
        return(str(int(h)) + " h "+str(int(min)) + " min "+str(int(s))+" s")

print("Finished in time ", formatTime(start))

'''
#safecheck
newl=np.load("lookuptable8qcell.npy")

for i in range(len(newl)):
    for k in range(len(newl[0,:])):
        if newl[i,k]==lutordered[i][k]:
            print("ok")
            print(newl[i][k],lutordered[i][k])
        else:
            print("MISTAKE HERE")
            print(newl[i][k],lutordered[i][k])
'''







