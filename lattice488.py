#
"""
Color code 4.8.8 lattice for the rescaling decoder

@author: Pedro Parrado
"""

from colorcode import *
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
from matplotlib.patches import Polygon
from matplotlib.collections import PatchCollection

class Code488(ColorCode):
    '''
    This class imports the general functions for the color code decoder
    and initializes a code with the setup for the 488 color code with
    9 qubits triangular cells
    |\
    |/|\
    |\|/|\

    First, we define some basic functions to translate the index in the list to the coordinates
    remember we consider only X stabilizers, and the coordinates of the lower left corner.

     2L           |/|\|/|\|/|\|
     2L-1        |\|/|\|/|\|/|
     .          |/|\|/|\|/|\|
     .         |\|/|\|/|\|/|
     1        |/|\|/|\|/|\|
     0       |\|/|\|/|\|/|
             0  1  ...   2L

    For the qubits, we consider the coordinate z to be:
                z = 0 : leftmost qubit
                z = 1 : rightmost qubit

    '''
    def qubitIndexToCoord(self,i):
        x = ( i%(self.L*4) ) // 2
        y = i // (4 * self.L)
        z = i %2
        return x,y,z
    def qubitCoordToIndex(self,x,y,z):
        # the %2L on x and y is there so that we can call this function for negative values of (x,y)
        return 2 * (x%(2*self.L)) + 4 * self.L * (y%(2*self.L)) + z
    def stabIndexToCoord(self,i):
        x = i % (2*self.L)
        y = i // (2*self.L)
        return x,y
    def stabCoordToIndex(self,x,y):
        return (x%(2*self.L)) + (y%(2*self.L)) * 2*self.L
    def qubitposition(self,index, plotQubitOffset = 0.3,returnMissingStab=False):
        ex = plotQubitOffset
        mex = 1-ex
        x,y,z = self.qubitIndexToCoord(index)
        # we add 1 to separate cells
        cellOffsetx = x//3
        cellOffsety = y//3
        missing=0
        if (x + y) % 2 == 0:
            if z == 0:
                x += ex
                y += ex
                missing=2
            if z == 1:
                x += mex
                y += mex
                missing=0
        else:
            if z == 0:
                x += ex
                y += mex
                missing=1
            if z == 1:
                x += mex
                y += ex
                missing=3

        if returnMissingStab:
            return x+cellOffsetx, y+cellOffsety, missing
        return x+cellOffsetx,y+ cellOffsety

    def __init__(self, m=1, p=0.01, shortcuts = False, tolerance = 1e-15,hardSplitting = False,
                 qubitTolerance = -1, splitTolerance = -1,useposterior = False):
        # m is the size of the code in terms of rescaling steps
        self.m = m

        self.p = p

        self.L = 3**m

        self.ncells = 4 * 3**(2*m -2)

        # variable deciding if we take prunning shortcuts when computing configurations
        self.prunningShortcuts = shortcuts

        # number of stabilizers
        self.Nstab = 4 * self.L ** 2

        # load lookuptable for cells
        self.lutcell = np.load("lookuptable18qcell.npy")
        # load lookuptable for the m=0 case
        self.lut0 = np.load("lookuptable488m0.npy")

        # inits the qubit pairs
        self.initqubits()

        # inits stabilizers
        self.initstabilizers()

        # init cells and splittings
        if m > 0:
            self.initcells()
            self.initsplits()


        # init logical operators
        self.initlogicops()

        # tolerance of the fixprob function
        self.tolerance = tolerance
        if qubitTolerance <0:
            self.qubitTolerance = self.tolerance
        else:
            self.qubitTolerance= qubitTolerance

        if splitTolerance<0:
            self.splitTolerance = self.tolerance
        else:
            self.splitTolerance = splitTolerance

        self.hardSplitting = hardSplitting
        self.useposterior = useposterior

        if self.m>0:
            for cell in self.cells:
                cell.tol = tolerance
                cell.useposterior = useposterior

        # inits the new code (only if m>0)
        self.initnewcode()

        self.setCutoff(tolerance,qubitTolerance,splitTolerance)
        # debugging
        self.Ncorrection = [0]*(self.m+1)

    def initqubits(self):
        '''
        Initializes the data structures for qubits and their probabilities
        for a 488 toric color code
        '''
        # Nq is the number of qubits in the code
        self.Nq = 8 * self.L**2 # value for the toric color code

        # q is the list of qubits in the code
        self.q = []
        for i in range(self.Nq // 2):
            self.q.append(Qubitpair(self.p))
            # set the index of the qubits in the qubit pair
            self.q[-1].q = [2 * i, 2 * i + 1]

        # main dictionary to address the qubits in the pairs
        # qd[index] = (index of the qubitpair, index of the qubit within the pair )
        self.qd = {}
        for i in range(self.Nq):
            self.qd[i] = (i // 2, i % 2)

        # aditional dictionaries to address qubits by coordinates easily
        self.qcoord = {}
        self.qindex = {}
        for i in range(self.Nq):
            # x = ( i%(self.L*4) ) // 2
            # y =  i // (4 * self.L)
            # z = i % 2
            x, y, z = self.qubitIndexToCoord(i)
            self.qcoord[i]  = (x, y, z)
            self.qindex[(x, y, z)] = i

        return

    def initstabilizers(self):
        '''
        Initializes the data structures for stabilizers
        for a 488 toric color code
        '''

        # stab is the list of the stabilizers in the code
        self.stabs = []
        # splits is the list of the splittings in the code
        self.splits = []


        # aditional dictionaries to address stabilizers by coordinates
        self.scoord = {}  # scoord[index] = (x,y)
        self.sindex = {}  # sindex[(x,y)] = index
        # same but for the splittings
        self.spcoord = {} # spcoord[index of split pair] = [(x1,y1), (x2,y2)] #(coords of stabs)
        self.spindex = {} # spindex[(x1,y1)] = index of split pair

        # list of indices of the corner stabilizers
        self.corners = []

        # coordinates of the stabilizer qubits for red stabilizers
        nx4 = [0, -1, -1,  0]
        ny4 = [0,  0, -1, -1]
        nz4 = [0,  1,  1,  0]

        # coordinates of the stabilizer qubits for 8-qubit stabilizers
        nx8 = [0,0, -1,-1, -1,-1,  0, 0]
        ny8 = [0,0,  0, 0, -1,-1, -1,-1]
        nz8 = [0,1,  0, 1,  0, 1,  0, 1]

        for i in range(self.Nstab):
            self.stabs.append(Stabilizer(i))
            x = i % (2*self.L)
            y = i // (2*self.L)
            self.scoord[i] = (x, y)
            self.sindex[(x, y)] = i

            # first, we check if its a 4-qubit or an 8-qubit stab
            if (x+y)%2==0: # then its a red stabilizer
                nx, ny, nz = nx4, ny4, nz4
            else:  # else its an 8-qubit stabilizer
                nx, ny, nz = nx8, ny8, nz8


            # we introduce the qubits into the stabilizer class
            for j in range(len(nx)):
                xq = (x + nx[j]) % (self.L*2)
                yq = (y + ny[j]) % (self.L*2)
                zq = nz[j]
                qind = self.qindex[(xq, yq, zq)]
                self.stabs[i].q.append(qind)

                # and the stabilizer index in the qubit
                pair, qubit = self.qd[qind]
                self.q[pair].s[qubit].append(i)

        if self.m>0:
            for i in range(self.Nstab):
                x = i % (2*self.L)
                y = i // (2*self.L)
                # conditions for it to be a splitting
                # split on the bottom side
                condh = (x % 3 == 1 and y % 3 == 0)
                # split on the left side of the cell
                condv = (x % 3 == 0 and y % 3 == 1)

                # this would be inside a 8qubit cell
                # cond3=(x%2==1 and y%2==1)
                if condh:
                    # we add a reference to this splitting
                    self.spcoord[len(self.splits)] = [(x, y),(x+1,y)]
                    self.spindex[(x, y)] = len(self.splits)
                    index2 = self.sindex[(x+1,y)]
                    # now we add the two stabilizers to the splitting
                    self.splits.append(Splitting([self.stabs[i],self.stabs[index2]]))
                if condv:
                    # we add a reference to this splitting
                    self.spcoord[len(self.splits)] = [(x, y),(x,y+1)]
                    self.spindex[(x, y)] = len(self.splits)
                    index2 = self.sindex[(x,y+1)]
                    # now we add the two stabilizers to the splitting
                    self.splits.append(Splitting([self.stabs[i],self.stabs[index2]]))

                # this would be a corner stab
                condcorner = (x % 3 == 0 and y % 3 == 0)
                if condcorner:
                    self.corners.append(i)

        return

    def initcells(self):
        '''
        Initializes the data structures for the cells in the code
        '''
        # cells is the list of cells in the code
        self.cells = []
        #ncells = self.Nq // 18  # a cell every 18 qubits
        ncells = self.ncells
        # aditional dictionaries to address stabilizers by coordinates
        self.ccoord = {}
        self.cindex = {}

        # noticed that the odd cells have mirrored notation with respect to the horizontal axis
        # coords of the qubits in even cells (z=0,1 for all)
        nxqeven = [0,1,2, 0,1,2, 0,1,2]
        nyqeven = [0,0,0, 1,1,1, 2,2,2]
        # coords of the qubits in odd cells (z=0,1 for all)
        nxqodd  = [0,1,2, 0,1,2, 0,1,2]
        nyqodd  = [2,2,2, 1,1,1, 0,0,0]

        # coords of the stabs in the cell
        # anticlockwise, starting with the one in the lower side of the cell
        # first, stabs in the border, then the 4 bulk stabs
        nxseven = [1, 2, 3, 3, 2, 1, 0, 0,   1, 2, 2, 1 ]
        nyseven = [0, 0, 1, 2, 3, 3, 2, 1,   1, 1, 2, 2 ]
        # mirrored if odd cell
        nxsodd =  [1, 2, 3, 3, 2, 1, 0, 0,   1, 2, 2, 1 ]
        nysodd =  [3, 3, 2, 1, 0, 0, 1, 2,   2, 2, 1, 1 ]


        # coords of the splits in the cell
        # follows the same order as the stabilizers
        nxspliteven = [1,  3, 1, 0]
        nyspliteven = [0,  1, 3, 1]
        # mirrored if odd cell
        # mirrored if odd cell
        nxsplitodd =  [1, 3, 1, 0]
        nysplitodd =  [3, 1, 0, 1]


        for i in range(ncells):
            # we add a new cell to the list
            self.cells.append(Cell(i, self.prunningShortcuts))

            # coordinates of the cell
            x = i % (self.L // (3/2)) * 3
            y = 3 * (i // (self.L // (3/2)))
            self.ccoord[i] = (x, y)
            self.cindex[(x, y)] = i

            '''
            Initialization of qubits in the cell
            '''

            # quick access to the cell in this step
            cell = self.cells[i]

            cell.lookuptabledata = self.lutcell

            # now we fill the qubits in the cell
            # even and odd cells alternate in a checkerboard pattern

            # even cells  |\|
            if (x+y)%2 == 0:
                nx, ny = nxqeven, nyqeven
                nxs,nys = nxseven, nyseven
                nxsplit, nysplit = nxspliteven, nyspliteven
                zqubits = [0,1]

            else: # odd cells |/|
                nx, ny = nxqodd, nyqodd
                nxs,nys = nxsodd, nysodd
                nxsplit, nysplit =nxsplitodd, nysplitodd
                zqubits = [0,1]

            for j in range(len(nx)):
                # coordinates of the qubit
                xq = (x + nx[j]) % (2*self.L)
                yq = (y + ny[j]) % (2*self.L)
                # index of the qubit
                indexq = self.qindex[(xq, yq, zqubits[0])]
                indexq2 = self.qindex[(xq, yq, zqubits[1])]
                # index of the qubitpair
                pairind, z = self.qd[indexq]
                # we add the pair to the cell
                cell.qp.append(self.q[pairind])
                # between both qubit pair references
                # we add a reference to its qubits in the cell dictionary
                cell.qd[2 * j] = (j, 0, indexq2)
                cell.qd[2 * j + 1] = (j, 1, indexq)

                # now we add the reference to the dictionary in cell
                cell.qout[indexq] = j * 2
                cell.qout[indexq2] = j * 2 + 1

            '''
            Initialization of stabilizers and splittings in the cell
            '''

            for j in range(len(nxs)):
                # coordinates of the stab
                #xs,ys = self.stabIndexToCoord(j)
                xs = (x + nxs[j]) % (2*self.L)
                ys = (y + nys[j]) % (2*self.L)

                # index of the stab
                indexs = self.sindex[(xs, ys)]

                # we add the stabilizer to the stab list in the cell
                cell.stab.append(self.stabs[indexs])

            for j in range(len(nxsplit)):
                # coordinates of the lower left stab in the splitting
                # xs,ys = self.stabIndexToCoord(j)
                xs = (x + nxsplit[j]) % (2 * self.L)
                ys = (y + nysplit[j]) % (2 * self.L)

                # index of the stab
                indexsplit = self.spindex[(xs, ys)]

                # we add the splitting to the split list in the cell
                cell.splits.append(self.splits[indexsplit])
            '''
            initialization of the cell logical operators
            '''
            cell.logicops = [[0, 3, 4,6, 12], [5, 11, 17, 13, 14]]
        return

    def initsplits(self):
        '''
        This function fills the splitting classes with the information
        from the cells involved and the qubits in each half
        '''

        '''
        first, we define the neighbouring qubits and cells, depending on 
        the stabilizer: horizontal or vertical
        '''
        # vertical split case:(cell0 left, cell1 right)
        nvx = [[-1, -1, -1], [0, 0, 0]]
        nvy = [[-1, 0, 0], [0, -1, -1]]
        nvz = [[1, 0, 1], [0, 1, 0]]
        nvc = [(-3, -1), (0, -1)]
        # horizontal split case:(cell0 up, cell1 down)
        nhx = [[-1, -1, 0], [0, 0, -1]]
        nhy = [[0, 0, 0], [-1, -1, -1]]
        nhz = [[0, 1, 0], [1, 0, 1]]
        nhc = [(-1, 0), (-1, -3)]

        for i in range(len(self.splits)):
            # spcoord[i] = [(x1,y1),(x2,y2)]
            x, y = self.spcoord[i][0]

            split = self.splits[i]
            split.index = i
            # we find if the splitting is horizontal or vertical

            if x % 3 == 0 and y % 3 == 1:
                # vertical splitting
                # add qubits
                for k in range(3):
                    for nc in range(2):
                        xq = (nvx[nc][k] + x) % (self.L*2)
                        yq = (nvy[nc][k] + y) % (self.L*2)
                        zq = nvz[nc][k]
                        qubitind = self.qindex[(xq, yq, zq)]
                        split.q[nc].append(qubitind)
                # add cells
                x0, y0 = nvc[0]
                x1, y1 = nvc[1]

            if x % 3 == 1 and y % 3 == 0:
                # horizontal splitting
                # add qubits
                for k in range(3):
                    for nc in range(2):
                        xq = (nhx[nc][k] + x) % (self.L*2)
                        yq = (nhy[nc][k] + y) % (self.L*2)
                        zq = nhz[nc][k]
                        qubitind = self.qindex[(xq, yq, zq)]
                        split.q[nc].append(qubitind)
                # add cells
                x0, y0 = nhc[0]
                x1, y1 = nhc[1]
            x0 = (x0 + x) % (self.L*2)
            y0 = (y0 + y) % (self.L*2)
            x1 = (x1 + x) % (self.L*2)
            y1 = (y1 + y) % (self.L*2)
            cell0 = self.cells[self.cindex[(x0, y0)]]
            cell1 = self.cells[self.cindex[(x1, y1)]]
            split.initcells([cell0, cell1])

    def initlogicops(self):
        '''
        initializes the logical operators
        '''
        qbits = [[],[],[],[]]
        for i in range(self.L*2):
            qbits[0].append( self.qubitCoordToIndex(0,i,0) )       #green vertical
            qbits[1].append( self.qubitCoordToIndex(0,i,1) )        #blue vertical
            qbits[2].append( self.qubitCoordToIndex(i,0,i%2))       #blue horizontal
            qbits[3].append( self.qubitCoordToIndex(i,0,(i+1)%2))   #green horizontal
        self.logicops = qbits

    def initnewcode(self):
        '''
        Initializes the newcode that will serve for the rescaling
        '''

        # newcode is the rescaled version of the code
        if self.m > 0:
            self.newcode = Code488(self.m - 1, shortcuts=self.prunningShortcuts, tolerance= self.tolerance,
                                   hardSplitting = self.hardSplitting, qubitTolerance=self.qubitTolerance,
                                   splitTolerance=self.splitTolerance, useposterior=self.useposterior)
            # we dont assign a value of p because every cell will be
            # assigned a individual p in the rescaling

    def rescalecell(self, cell):
        '''
        Method to rescale the 18 qubit cell and find the matrix of
        probabilities with soft rescaling

        to compute the probability of rescaling, we need several ingredients:

            the chosen correction c

            the different permutations of stabilizers applied to the correction
            the different permutations of logical operators applied to the correction

            all possible choices of splittings, with their probabilities and the support
            of the stabilizers in the cell

        Once we have all of that, we can compute the rescaling probability for
        each case of logical operators:  Id, L0, L1, L0·L1

        '''
        # the output will be stored in
        pmatrix = [[0, 0], [0, 0]]
        # [["00","01"],["10","11"]] # probability of each logical error combination

        # first, we find the correction
        c = []
        for qpair in cell.qp:
            c += qpair.c
        c = np.array(c)
        # now c should be an array with the correction applied

        # now, we need to add the different permutations of stabilizers and logic ops
        # for that, we can take the elements of the lookuptable for the syndrome with all zeros

        syndrome0 = np.zeros(len(cell.stab))
        stabLogPermutations = cell.lookuptable(syndrome0)

        # these permutations are ordered in the following way:
        # 16 permutations of the stabilizers
        # 16 permutations of the stabilizers + L0
        # 16 permutations of the stabilizers + L1
        # 16 permutations of the stabilizers + L2

        # they are in string format (e.g. "001010010")
        # now, we combine those permutations of stabs and log.ops. with the chosen correction:
        configsCSL = []
        for permutation in stabLogPermutations:
            newConfiguration = np.array(c)
            for q in range(len(permutation)):
                newConfiguration[q] = (c[q] + int(permutation[q]))%2
            configsCSL.append(newConfiguration)

        # now configsCSL contains all combinations of the correction plus stabilizers plus logic ops
        # given a configuration "i" in this list, we know the logical operator applied by doing:
        # tableLogicOperators[i//16]
        # indices of the matrix of probabilities
        tableLogicOperators = [[0, 0], [1, 0], [0, 1], [1, 1]]
        #["No logic op", "Logic op 0", "Logic op 1", "Logic ops 0,1"]
        #[correctionIndex//16]

        # now we can run through the split possibilities to compute the
        # soft rescaling probabilities
        qstabs, pstabs = cell.splitconfGenerator()

        # we loop over all possible split choices
        for isplit in range(len(qstabs)):
            '''
            now we compute the probability of each configuration
            by adding to the configuration the stabilizers
            For each stabilizer configuration from qstabs that we
            add, we have to compute the probabilities of each element
            of the matrix
            '''
            thisSplitSupport = qstabs[isplit]
            thisSplitProbability = pstabs[isplit]


            # now, we loop over all possible logic operator combinations
            partialSums = np.zeros(4)
            for ilogic in range(4):
                # and loop over all stabilizer permutations
                for istab in range(16):
                    # thisConfiguration = thisStabPermutation + thisSplitSupport
                    thisConfig = configsCSL[istab + ilogic*16] + thisSplitSupport
                    thisProbability = cell.pconf(thisConfig)

                    # now we add the probability of that configuration to the partial sum
                    partialSums[ilogic] += thisProbability


            # the determinant is the sum for all logic operator permutations
            determinant = self.fixprob(np.sum(partialSums))

            # now we can compute, for each logic permutation, the rescaling probability
            # associated to this particular choice of splittings
            for ilogic in range(4):
                i1, i2 = tableLogicOperators[ilogic]
                # p(splitChoice) * p(L0 *i1, L1*i2) / sum_{L0,L1}( p(L0,L1) )
                pmatrix[i1][i2] += self.fixprob( thisSplitProbability * partialSums[ilogic] / determinant)

        # normalize the probabilities
        normaliationF = 0
        for ilogic in range(4):
            i1, i2 = tableLogicOperators[ilogic]
            normaliationF+= self.fixprob(pmatrix[i1][i2],self.qubitTolerance)
        for ilogic in range(4):
            i1, i2 = tableLogicOperators[ilogic]
            pmatrix[i1][i2] = self.fixprob(pmatrix[i1][i2]/normaliationF,self.qubitTolerance)

        return pmatrix


    def plot(self, latticeGrid=True, qubits=False, syndrome=True, stabCircleSize = 10,
             indexStabilizers=False, logicops=False, stabSupport = [], probabilities=False,
             correction=False,  error=True, splitting=False, figsize=(15,15),markersize = 10,
             printp = False, printpsize = 10,reset= True,printChoiceIndex = False):
        '''

        :param lattice:  decides if you plot the grid of the code lattice.
        :param qubits:
        :param syndrome: plots the syndrome over the stabilizers
        :param stabCircleSize: size of the marker for stabilizers
        :param indexStabilizers: plots the index of each stabilizer over them
        :param logicops:
        :param correction:
        :param cells:
        :param error:
        :param splitting:
        :return:
        '''


        L = self.L
        m = self.m
        # lattice plotter
        if reset:
            plt.clf()
            plt.figure(figsize = figsize)

        _, ax = plt.subplots(figsize = figsize)
        plt.axis('off')
        #ax = plt.axes

        if latticeGrid:
            # plot the lattice grid
            if m==0:
                x = [1,2,2,0,0,1,2,1,0,1,1,0,2]
                y = [0,0,2,2,0,0,1,2,1,0,2,1,1]
                plt.plot(x,y, 'black')  # horizontal
            else:
                ncellsPerLine =2 * 3**(m-1)
                for ix in range(ncellsPerLine):
                    for iy in range(ncellsPerLine):
                        # coordinates of the bottom-left corner of the cell
                        xcell = ix*3 +ix
                        ycell = iy*3 +iy

                        # square surrounding the cell
                        plt.plot([xcell,xcell+3,xcell+3,xcell,xcell],
                                 [ycell,ycell, ycell+3,ycell+3,ycell],'k')
                        # main grid of the cell
                        plt.plot([xcell+1,xcell+1], [ycell,ycell+3],'k')
                        plt.plot([xcell+2,xcell+2], [ycell,ycell+3],'k')
                        plt.plot([xcell,xcell+3], [ycell+1,ycell+1],'k')
                        plt.plot([xcell,xcell+3], [ycell+2,ycell+2],'k')

                        #depending on the orientation of the cell
                        if (ix+iy)%2==0:
                            # cell type: |\|
                            plt.plot([xcell,xcell+3], [ycell+3,ycell],'k',linewidth=5)
                            plt.plot([xcell,xcell+2], [ycell+1,ycell+3],'k')
                            plt.plot([xcell+1,xcell+3], [ycell,ycell+2],'k')
                            plt.plot([xcell,xcell+1], [ycell+1,ycell+0],'k')
                            plt.plot([xcell+2,xcell+3], [ycell+3,ycell+2],'k')
                        else:
                            # cell type: |/|
                            plt.plot([xcell,xcell+3], [ycell,ycell+3],'k',linewidth=5)
                            plt.plot([xcell,xcell+2], [ycell+2,ycell],'k')
                            plt.plot([xcell+1,xcell+3], [ycell+3,ycell+1],'k')
                            plt.plot([xcell,xcell+1], [ycell+2,ycell+3],'k')
                            plt.plot([xcell+2,xcell+3], [ycell,ycell+1],'k')


        # stabilizer plotter

        def whichRGB( stabIndex):
            x, y = self.stabIndexToCoord(stabIndex)
            if (x + y) % 2 == 0:
                # stabcolor = 'red'
                return 0
            elif x % 2 == 0:
                # stabcolor = 'green'
                return 1
            else:
                # stabcolor = 'blue'
                return 2

        # plot the stabilizers and syndrome
        colors = ["r", "b", "g"]
        if (syndrome):
            for i in range(self.Nstab):
                x0,y0 = self.stabIndexToCoord(i)
                col = colors[whichRGB(i)]
                # additional offset due to the cell separation
                x = x0+x0//3
                y = y0+y0//3
                x  = [x]
                y = [y]

                # add duplicate if it is on the bottom left edge
                if x0%3==0 and x0>0:
                    x.append(x[0] -1)
                    y.append(y[0])
                    if y0==0:
                        x.append(x[0]-1 )
                        y.append(y[0] + 2 * L + max((2 * L) // 3 - 1, 0))
                if y0%3==0 and y0>0:
                    x.append(x[0] )
                    y.append(y[0] -1)
                    if x0==0:
                        x.append(x[0]+ 2 * L + max((2 * L) // 3 - 1, 0))
                        y.append(y[0] -1 )
                if x0%3==0 and y0%3==0 and x0>0 and y0>0:
                    x.append(x[0] -1)
                    y.append(y[0] -1)


                # add periodic boundary duplicate
                if (x[0] == 0):
                    x.append(x[0] + 2*L + max((2*L)//3-1,0))
                    y.append(y[0])
                if (y[0] == 0):
                    x.append(x[0])
                    y.append(y[0] + 2*L + max((2*L)//3-1,0))
                if (y[0] == 0 and x[0] == 0):
                    x.append(x[0] + 2*L + max((2*L)//3-1,0))
                    y.append(y[0] + 2*L + max((2*L)//3-1,0))

                plt.plot(x, y, '.', color='k', marker="o", markersize=stabCircleSize+1)
                plt.plot(x, y, '.', color=col, marker="o", markersize=stabCircleSize-3)
                ''''''
                # syndrome plotter
                if (self.stabs[i].vmeasured == 1):
                    plt.plot(x, y, '.', color="black", marker="$o$", markersize=stabCircleSize+3)
                    #plt.plot(x, y, '.', color="black", marker="$o$", markersize=11)
                    plt.plot(x, y, '.', color="yellow", marker="$o$", markersize=stabCircleSize)
                ''''''
                if indexStabilizers:
                    plt.plot(x, y, '.', color="black", marker="$" + str(i) + "$",
                             markersize=stabCircleSize+1)
                    plt.plot(x, y, '.', color="yellow", marker="$" + str(i) + "$",
                             markersize=stabCircleSize-1)


        # qubit plotter

        if qubits:
            for i in range(self.Nq):
                x,y = self.qubitposition(i)
                plt.plot(x, y, '.', color="k", marker="o", markersize=18)
                plt.plot(x, y, '.', color="yellow", marker="$"+str(i)+"$", markersize=14)
        if probabilities:
            # we will plot a coloured circle over each qubit with a colour depending on its probability
            def colorOfP(p):
                red = np.array(mpl.colors.to_rgb('red'))
                green = np.array([0.4,0.93,0.2])
                blue = np.array(mpl.colors.to_rgb('blue'))
                white = np.array([.99,1.,.99])

                redp = 1
                bluep=0.1
                greenp = 1e-2
                whitep = self.qubitTolerance

                if p>bluep:
                    c1 = blue
                    c2 = red
                    mix = (p-bluep)/(redp-bluep)
                elif p>greenp:
                    c1 = green
                    c2 = blue
                    mix = np.abs((p-greenp)/(bluep-greenp))
                else:
                    c1 = white
                    c2 = green
                    #mix = np.abs((p-whitep)/(greenp-whitep))
                    mix = np.abs((np.log(p+whitep)-np.log(whitep))/(np.log(greenp)-np.log(whitep)))
                return mpl.colors.to_hex((1-mix)*c1 + mix*c2)

            for i in range(self.Nq):
                xq,yq,missing = self.qubitposition(i,returnMissingStab=True)

                pair, qubit = self.qd[i]
                pi = self.q[pair].psingle[qubit]
                #plt.plot(x, y, '.', color=colorOfP(pi), marker="o", markersize=markersize*3,alpha=0.2)

                #%%%

                points = []
                patches = []


                x = int(xq)
                y = int(yq)
                points= [[x,y],
                         [x+1, y],
                         [x+1, y+1],
                         [x,y+1]]
                points.pop(missing)

                polygon = Polygon(points, True)
                patches.append(polygon)

                p = PatchCollection(patches, alpha=0.2) #, cmap='jet')
                p.set_color(colorOfP(pi))
                #p.set_array(np.array(colors))
                ax.add_collection(p)



                ###
                if printp:
                    pstr = "%.1g"%pi
                    plt.plot(xq, yq,  color='k', marker= "$"+pstr+"$", markersize=printpsize, alpha=0.8)

        if logicops:
            for i in self.logicops[0]:
                x,y = self.qubitposition(i)
                plt.plot(x, y, '.', color="red", marker="$H$", markersize=14)
            for i in self.logicops[1]:
                x,y = self.qubitposition(i)
                plt.plot(x, y, '.', color="blue", marker="$H$", markersize=14)
            for i in self.logicops[2]:
                x,y = self.qubitposition(i)
                plt.plot(x, y, '.', color="red", marker="$V$", markersize=14)
            for i in self.logicops[3]:
                x,y = self.qubitposition(i)
                plt.plot(x, y, '.', color="blue", marker="$V$", markersize=14)


        # error plotter
        if error:

            for i in range(self.Nq):
                pair, qubit = self.qd[i]
                erri = self.q[pair].e[qubit]

                if erri == 1:
                    x,y = self.qubitposition(i)
                    col = "r"
                    plt.plot(x-0.1, y, color=col, marker="$X$", markersize=markersize)

        # correction plotter
        if correction:

            for i in range(self.Nq):
                pair, qubit = self.qd[i]
                corri = self.q[pair].c[qubit]
                if corri == 1:
                    x,y = self.qubitposition(i)
                    plt.plot(x + .15, y, color='blue', marker="$C$", markersize=markersize)

        # splitting plotter
        if splitting and self.m > 0:
            pass

            mark = ["$+$", "$-$"]
            for i in range(len(self.splits)):
                (x1, y1),(x2,y2) = self.spcoord[i]
                v1u,v2u = self.splits[i].getvalue(cellInternalIndex=0, measuredValue = True)
                v1d,v2d = self.splits[i].getvalue(cellInternalIndex=1, measuredValue = True)

                if x1 % 3 == 1 and y1 % 3 == 0:
                    # horizontal splitting
                    x = x1+x1//3
                    y = y1+y1//3 -1
                    markdown=mark[v1d]
                    markup=mark[v1u]
                    plt.plot(x+.1 , y + 0.4, color="black", marker="o", markersize=2+markersize)
                    plt.plot(x+.1 , y + 0.4, color="yellow", marker=markdown, markersize=markersize)
                    plt.plot(x+.1 , y + 0.7, color="black", marker="o", markersize=2+markersize)
                    plt.plot(x+.1 , y + 0.7, color="yellow", marker=markup, markersize=markersize)
                    x = x2+x2//3
                    y = y2+y2//3-1
                    markdown=mark[v2d]
                    markup=mark[v2u]
                    plt.plot(x , y + 0.4, color="black", marker="o", markersize=2+markersize)
                    plt.plot(x , y + 0.4, color="yellow", marker=markdown, markersize=markersize)
                    plt.plot(x , y + 0.7, color="black", marker="o", markersize=2+markersize)
                    plt.plot(x , y + 0.7, color="yellow", marker=markup, markersize=markersize)

                if x1 % 3 == 0 and y1 % 3 == 1:
                    # vertical splitting
                    x = x1+x1//3-1
                    y = y1+y1//3
                    markleft=mark[v1u]
                    markright=mark[v1d]
                    plt.plot(x+ 0.3 , y , color="black", marker="o", markersize=2+markersize)
                    plt.plot(x+ 0.3 , y, color="yellow", marker=markleft, markersize=markersize)
                    plt.plot(x+ 0.6 , y, color="black", marker="o", markersize=2+markersize)
                    plt.plot(x+ 0.6 , y, color="yellow", marker=markright, markersize=markersize)


                    x = x2+x2//3-1
                    y = y2+y2//3
                    markright=mark[v2d]
                    markleft=mark[v2u]
                    plt.plot(x+ 0.3 , y, color="black", marker="o", markersize=2+markersize)
                    plt.plot(x+ 0.3 , y, color="yellow", marker=markleft, markersize=markersize)
                    plt.plot(x+ 0.6 , y, color="black", marker="o", markersize=2+markersize)
                    plt.plot(x+ 0.6 , y, color="yellow", marker=markright, markersize=markersize)


                # plot the index of the choice in the middle
                if printChoiceIndex:
                    x = x1+x1//3
                    y = y1+y1//3
                    choice = self.splits[i].choice
                    plt.plot(x - .5, y - 0.5, color="black", marker="o", markersize=2+markersize)
                    plt.plot(x - .5, y - 0.5, color="yellow", marker="$" + str(choice) + "$", markersize=markersize)
                '''
                ptext = str(np.round(self.splits[i].ps, 3))
                plt.plot(x, y - 0.3, color="black", marker="$" + ptext + "$", markersize=29)
                plt.plot(x, y - 0.3, color="black", marker="$" + ptext + "$", markersize=24)
                plt.plot(x, y - 0.3, color="yellow", marker="$" + ptext + "$", markersize=27)'''

        # stab support (debugging)
        if stabSupport:
            for stabInd in stabSupport:
                i = stabInd
                x0,y0 = self.stabIndexToCoord(i)
                col = 'yellow'
                # additional offset due to the cell separation
                x = x0+x0//3
                y = y0+y0//3
                x  = [x]
                y = [y]

                # add duplicate if it is on the bottom left edge
                if x0%3==0 and x0>0:
                    x.append(x[0] -1)
                    y.append(y[0])
                    if y0==0:
                        x.append(x[0]-1 )
                        y.append(y[0] + 2 * L + max((2 * L) // 3 - 1, 0))
                if y0%3==0 and y0>0:
                    x.append(x[0] )
                    y.append(y[0] -1)
                    if x0==0:
                        x.append(x[0]+ 2 * L + max((2 * L) // 3 - 1, 0))
                        y.append(y[0] -1 )
                if x0%3==0 and y0%3==0 and x0>0 and y0>0:
                    x.append(x[0] -1)
                    y.append(y[0] -1)


                # add periodic boundary duplicate
                if (x[0] == 0):
                    x.append(x[0] + 2*L + max((2*L)//3-1,0))
                    y.append(y[0])
                if (y[0] == 0):
                    x.append(x[0])
                    y.append(y[0] + 2*L + max((2*L)//3-1,0))
                if (y[0] == 0 and x[0] == 0):
                    x.append(x[0] + 2*L + max((2*L)//3-1,0))
                    y.append(y[0] + 2*L + max((2*L)//3-1,0))

                plt.plot(x, y, '.', color='k', marker="o", markersize=stabCircleSize+5)
                plt.plot(x, y, '.', color=col, marker="o", markersize=stabCircleSize+2)

                qubits = self.stabs[stabInd].q
                for q in qubits:
                    x,y = self.qubitposition(q)
                    plt.plot(x, y, '.', color="k", marker="o", markersize=18)
                    plt.plot(x, y, '.', color="yellow", marker="$"+str(q)+"$", markersize=14)


        return ""













