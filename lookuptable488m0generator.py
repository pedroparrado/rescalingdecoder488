"""
Lookuptable for the triangular cell in the 488 color code

@author: pedro
"""

# import numpy as np
import time

start = time.time()

# the lookuptable is a list of sets, one per syndrome
# each set has the combinations of errors that trigger it
# for example, 100100101000 would mean errors in the
# qubits 0,3, 6 and 8


lut = []
for i in range(2 ** 12):
    lut.append(set())

'''
Notation for qubits starting from the bottom left, increasing to the right by rows.
Notation for the stabilizers starting bottom left, counter-clockwise,
first the splittings and second the bulk stabilizers

 |4 /|\7 |          
 |/5 | 6\|           | /  |\  | 
 |\1 |2 /|          2|/ 3 | \ | 
 |0 \|/ 3|           |\   | / | 
                     | \  |/  | 
                     0    1

Notation from stabilizers starting from the bottom left (shared by qubits 0-3) 
and following similar ordering as the qubits: left to right, bottom to top

Note that stabilizers 1 and 2 are equivalent. We keep both of them
'''

# stabilizers correspondent to each qubit
stabsInQubit = [
    [0,1,2],  # 0 (bottom-left corner)
    [1,2,3],  # 1
    [1,2,3],  # 2
    [0,1,2],  # 3
    [0,1,2],  # 4
    [1,2,3],  # 5
    [1,2,3],  # 6
    [0,1,2]  # 7
]
# qubits correspondent to each stabilizer
stabs = [
    [0, 3,4,7],  # 0
    [1,2,3,4,5,6,7],  # 1
    [1,2,3,4,5,6,7],  # 2
    [1,2,5,6]  # 3
]

'''
Now, we will define the logical operators and bulk stabilizers.
For each syndrome, given a valid correction, 
we can generate the rest of possible corrections compatible with
that syndrome by adding a combination of logical operators and 
bulk stabilizers.

In the following lines, we will define this set of possible corrections
for the syndrome ++++++

'''

# logical operators
Lops =  [set([1,5]),           # Vertical 1
         set([0,4]),           # Vertical 2
         set([4,7]),           # Horizontal 1
         set([5,6])]           # Horizontal 2

# bulk stabilizers (only 2 of them are really independent)
bulkStabs = [set([0, 3,4,7]),  # 0
             set([1,2,5,6])]  # 3

# bulk stabilizer permutations

bulkStabPermutations = []
for i in range(2):
    for j in range(2):

        loops = [i, j]
        per = set([])

        for index in range(len(loops)):
            if loops[index] > 0:
                per = bulkStabs[index] ^ per

        bulkStabPermutations.append(per)

# logic operator permutations
logicOpPermutations = []

for i in range(2):
    for j in range(2):
        for k in range(2):
            for l in range(2):

                loops = [i,j,k,l]
                per = set([])

                for index in range(len(loops)):
                    if loops[index] > 0:
                        per = Lops[index] ^ per

                logicOpPermutations.append(per)

'''
Now, we will store in generators a list with all the possible sets of 
equivalent corrections, as a list of sets containing the qubits acted on

e.g.

generators = [set(),
 {6, 7, 8, 9, 12, 13, 14, 15},
 {9, 10, 15, 16},
 {6, 7, 8, 10, 12, 13, 14, 16},
 {2, 3, 4, 5, 8, 9, 10, 11},
 ...
 ]

 It is important to note that the possible corrections in 
 the generators list will be ordered in the following manner:

 first,  all 2^4 combinations equivalent to NO logical operator applied
 second, all 2^4 combinations equivalent to L0 being applied
 third,  all 2^4 combinations equivalent to L1 being applied
 last ,  all 2^4 combinations equivalent to L0 and L1 being applied

L0 is the logical operator of the lower left cell
L1 is the logical operator of the top right cell

'''
generators = []

for logicOp in logicOpPermutations:
    for stabPermutation in bulkStabPermutations:
        generators.append(logicOp ^ stabPermutation)

'''
Here, we define some functions to apply these operators in 
set format (e.g. op = [1,5,6,9])
to a state in string format (e.g. state = "001001010000010").

We also define a function with which we can generate all possible
valid corrections for a syndrome. It requires an initial valid 
correction for that syndrome, and using it, the function
genstates(state) will apply all possible combinations of the 
logical ops and stabilizers, as defined in the list "generators"
'''


def applyop(op, state):
    '''
    Toggles qubits in state "state" when applying an operator "op".
    the operator op is a list of qubits affected,
    state is a string of 01101010, which is
    going to be modified
    '''
    state2 = ''
    for i in range(len(state)):
        newq = int(state[i])
        if i in set(op):
            newq = (newq + 1) % 2
        state2 += str(newq)
    return state2


def genstates(correction):
    '''
    Given a valid correction for a syndrome, generates the full list of possible
    corrections for that syndrome. To do that, it applies all possible permutations
    of logical operators and bulk stabilizers.
    :param correction: a valid correction in string format (e.g. "001001010010")
    :return: a list with all possible corrections (e.g. ["01010","101001",...] )
    '''

    s = list()
    for gen in generators:
        s.append(applyop(gen, correction))
    return s


'''
Now, we will fill the lookuptable by finding a solution for each syndrome, 
and then applying genstates to it (which will apply all permutations of logic
operators and bulk stabilizers)

To find the solutions, we will go first through all possible error combinations
and generate an initial disordered lookuptable lut
'''

# initialization of the first unordered lookup table as a list of sets
lut = []
nstabs = 4
for i in range(2 ** nstabs):
    lut.append(set())

nqubits = 8
for i in range(2 ** nqubits):
    # these two lines generate all possible binary combinations
    er = bin(i)[2:]
    er = '0' * (nqubits - len(er)) + er

    syn = [0] * nstabs

    # now, we check the syndrome by going through each qubit and
    # changing the parity of the affected stabilizers
    for q in range(nqubits):
        if er[q] == '1':
            for s in stabsInQubit[q]:
                syn[s] = (syn[s] + 1) % 2

    # now we find the index of this syndrome using its translation to binary
    synb = ''
    for j in syn:
        synb += str(j)
    index = int(synb, 2)

    # and add this error configuration to the lookuptable
    lut[index].add(er)

'''
# now, we will go thorugh all elements in the lookuptable, and we will
# reorder them so that they apply logical ops in order 
(L0,L1) => (0,0),(1,0),(0,1),(1,1)

we will use the error configuration with the minimum amount of errors as 
the reference, to then add to it all possible stab and logicOp permutations
with the function genstates

The ordered lookuptable lutordered is now a list of lists
'''


def sumc(c):
    # number of errors in a configuration
    s = 0
    for e in c:
        s += int(e)
    return s


# ordered lookuptable
lutordered = []
for unordered in lut:
    # unordered corresponds to the set of configurations in the lookuptable
    # for a given syndrome

    # we will find the one with the least amount of errors
    mine = 900  # initial minimum or errors, way greater than nqubits
    minc = []  # configuration with the minimum amount of errors

    for conf in unordered:
        nerrors = sumc(conf)
        '''
        if nerrors==mine:
            print("2 error configurations with the same amount of errors")
            print(conf)
            print(minc)
        '''
        if nerrors < mine:
            mine = sumc(conf)
            minc = conf
    # once we found the configuration with the minimum ammount of errors,
    # we generate all the configurations again in order
    lutordered.append(genstates(minc))

# last thing to do, store the lookuptable

import numpy as np

np.save("lookuptable488m0", lutordered)


def formatTime(start):
    t = time.time() - start
    if t < 60:
        return str(round(t, 2)) + " s"
    elif t < 3600:
        min = t // 60
        s = t - min * 60
        return (str(int(min)) + " min " + str(int(s)) + " s")
    else:
        h = t // 3600
        min = (t - h * 3600) // 60
        s = t - min * 60 - h * 3600
        return (str(int(h)) + " h " + str(int(min)) + " min " + str(int(s)) + " s")


print("Finished in time ", formatTime(start))

'''
#safecheck
newl=np.load("lookuptable8qcell.npy")

for i in range(len(newl)):
    for k in range(len(newl[0,:])):
        if newl[i,k]==lutordered[i][k]:
            print("ok")
            print(newl[i][k],lutordered[i][k])
        else:
            print("MISTAKE HERE")
            print(newl[i][k],lutordered[i][k])
'''







