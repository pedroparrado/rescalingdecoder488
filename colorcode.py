"""
Color code decoder: Code definition and main class
Adapted for the 488 lattice
@author: pedro parrado
"""
from qubit import *
from stabilizer import *
from cell import *
import numpy as np
from multiprocessing import Pool
import time


class ColorCode:
    '''
    This class contains information about a cell in the code. This information
    includes the indexes of the qubits, stabilizers and splittings involved
    in the cell, the indexes of the neighboring cells and a lookuptable.
    The class also contains methods useful for the decoder.
    '''

    def version(self):
        return 10
    ### Main initialization
    def __init__(self, m=1, p=0.01):
        # m is the size of the code in terms of splitting steps
        self.m = m

        # L is the number of periodic cells
        #   |/|\|
        #  |\|/|
        self.L = 3**m
        # Nq is the number of qubits in the code
        self.Nq = 8 * self.L**2  # value for the 488 toric color code

        # q is the list of qubit pairs in the code
        self.q = []
        for i in range(self.Nq / 2):
            self.q.append(Qubitpair(p))

        # qd is a dictionary to access the single qubits
        self.qd = {}
        '''
        example: qd[3]=(1,1)        
        pair,qubit=qd[qubitindex]
        self.q[pair].psingle[qubit]
        '''
        # stab is the list of the stabilizers in the code
        self.stabs = []
        # splits is the list of the splittings in the code
        self.splits = []
        # list of the indices of the stabilizers that are in the corners
        self.corners = []
        '''
        #ideally one would initialize those lists as: 

        for i in range(self.Nstab):
            self.stab.append(Stabilizer())

        for i in range(self.Nsplit):
            self.split.append(Split())          
        '''
        # cells is the list of cells in the code
        self.cells = []

        # list with the logical operators, that are lists of qubit indices
        self.logicops = []

        # lookup table for the m=0 case
        self.lut0 = []

        # newcode is the rescaled version of the code
        if m > 0:
            self.newcode = ColorCode(m - 1, p)
        self.hardSplitting = False
        self.tolerance = 1e-20
        self.qubitTolerance=1e-5
        self.splitTolerance = 1e-5

        # debugging
        self.Ncorrection = [0]*(self.m+1)

        # variable that determines if the sum of probabilities of all split choices can be used
        # to modify the probability of a split configuration
        self.useposterior = False

    ### Decoder Master Class

    def decoder(self, cornerupdate=True, nsteps=1, nmean=5, niterBP=3, renormalizeOn = False, fixp = -5,
                debugText = False):
        '''
        This function applies the entire rescaling decoding algorithm to the code
        Returns the booleans (Any, [L1,L2,L3,L4]), that take the value 1 if
        a logical operator has been applied as a result of Error + Correction

            cornerupdate: boolean, activates the corner update rule

            variables for the splitting updates:
                nmean: number of updates between mean value steps
                nsteps: number of cycles of nmean steps

            niterBP: number of iterations for the Belief Propagation algorithm

        '''

        if debugText:
            startTime = time.time()

            print("Starting decoder for m = ",self.m)

        # if we are fixing the probability of the qubits, we set it to the initial value given in the input
        if fixp>0:
            for q in self.q:
                q.setpsingle(fixp,fixp)




        # first, we check if we are finished
        if self.m == 0:
            self.decoderm0()
            if debugText:
                print("     Time: ", time.time()-startTime)
                print("Finished decoder for m=0")

            return

        if debugText:
            print("Starting Belief Propagation")
            nextTime = time.time()

        # belief propagation prior update
        if niterBP > 0:
            self.beliefPropagation(niterBP)

        if debugText:
            print("     Time: ", time.time()-nextTime)
            timebp = time.time()-nextTime
            nextTime = time.time()
            print("Starting Corner Updates")
        # second, update corners
        if cornerupdate:
            self.updatecorners()


        if debugText:
            print("     Time: ", time.time()-nextTime)
            timeCornerUpdates = time.time()-nextTime
            nextTime = time.time()
            print("Starting Splitting Updates")
        # splitting update
        self.splitinit()
        self.splitupdates(nsteps, nmean)


        if debugText:
            print("     Time: ", time.time()-nextTime)
            timeSplits = time.time()-nextTime
            nextTime = time.time()
            print("Decoding cells")
        # find corrections and rescaling probability
        self.decodecells()


        if debugText:
            print("     Time: ", time.time()-nextTime)
            timeCellD = time.time()-nextTime
            nextTime = time.time()
            print("Starting Rescaling")
        # find rescaling
        self.rescaling()

        if debugText:
            print("     Time: ", time.time()-nextTime)
            timeSoftResc = time.time()-nextTime
            nextTime = time.time()
            print("Starting Decoder next size: m=",self.m-1)

        # decode the new code

        # renormalization of qubit probabilities: map probabilities to higher probabilities
        if self.m > 1 and renormalizeOn:
            self.newcode.renormalizeProbabilities()

        self.newcode.decoder(cornerupdate,nsteps,nmean,niterBP,renormalizeOn)

        if debugText:
            print("Time: ", time.time()-nextTime)
            timeNextSize = time.time()-nextTime
            nextTime = time.time()
            print("Starting backscaling")
        # rescale back
        self.backscale()
        if debugText:
            print("     Time: ", time.time()-nextTime)
            print("Decoder finished")
            print("     Total time: ", time.time()-startTime)
            timeTotal = time.time()-startTime
        if debugText:
            # list of all the timings, the 1 at the end is the number of samples
            #          [ 0       1                 2             3       4           5            6
            timelist = [timebp,timeCornerUpdates,timeSplits,timeCellD,timeSoftResc,timeNextSize,timeTotal,1]
            return np.array(timelist)
        # check that the solution solves the syndrome
        self.checksyndrome()

        return self.check()

    ###Decoder Functions

    def decoderm0Simple(self):
        # finds the correction in the lookuptable with the maximum probability,
        # without taking into account equivalent configurations or
        # the possibility of several corrections with equal probability

        lookupTable = self.lut0

        assert self.m == 0, "This decoder should only be called for m==0"

        # first, we find the index for the lutable
        synb = ''
        for stab in self.stabs:
            synb += str(stab.v)
        syndromeIndex = int(synb, 2)  # binary to integer

        # We look for the correction with maximum probability
        pmax = -2
        bestC = []

        for correction in lookupTable[syndromeIndex]:

            # we compute the probability of this correction
            prob = self.pconf(correction)

            if prob > pmax:
                pmax = prob
                bestC = correction

        # checking correction
        assert len(bestC) == self.Nq, "Correction should have 8 digits, but has " + str(len(bestC))

        # Apply the correction to the qubits
        for i in range(len(bestC)):
            pair, qubit = self.qd[i]
            self.q[pair].c[qubit] = int(bestC[i])
        return

    def decoderm0(self, deltaP = 1e-5):
        '''
        Finds a correction in the lookuptable for which its equivalence class has maximum probability:
        for each correction, computes the probability of the correction AND the correction + stabilizer permutations

        In addition, checks if there are several classes with similar probability, from which it picks at random

        input: deltaP -> tolerance to consider classes with similar probability
        '''


        assert self.m == 0, "This decoder should only be called for m==0"

        # first, we find the index for the lutable
        synb = ''
        for stab in self.stabs:
            synb += str(stab.v)
        syndromeIndex = int(synb, 2)  # binary to integer

        lookupTable = self.lut0[syndromeIndex]
        # We look for the correction with maximum probability
        pBest = -2
        bestC = []

        # since there are 2 stabs, we have to divide the #configs by 2^2
        numberOfClasses = len(lookupTable)//4

        for iclass in range(numberOfClasses):
            bestInClass = []
            auxPbestInClass=-3 #probability of the best in class

            # we compute the probability of all 4 elements in this class
            pClass = 0
            for k in range(4):
                correction = lookupTable[k + iclass*4]
                # we compute the probability of this correction
                prob = self.pconf(correction)
                pClass += prob
                # we store the best of this class
                if prob>auxPbestInClass:
                    auxPbestInClass = prob
                    bestInClass = correction

            # now that we have the best in this class, we check if it is better than the current best
            # up to a certain tolerance
            #if pClass > pBest*(1-deltaP):
            if pClass > pBest*(1-deltaP):

                # we know that the probability of this class is higher than bestP-deltaP
                # we want to know if it is close to the previous, or if it is a new best

                if pClass < pBest*(1+deltaP):
                    # in this case, this configuration has a similar pClass to the previous best, so we add it
                    bestC.append(bestInClass)
                else:
                    # otherwise, we have a new Best correction
                    pBest = pClass
                    bestC = [bestInClass]

        # now, bestC is a list with the best corrections found. Ideally, should contain only 1 correction

        if len(bestC)>1:
            # we choose one of the best corrections at random
            finalCorrection = bestC[np.random.randint(len(bestC))]
        else:
            # or pick the only Best correction found
            finalCorrection = bestC[0]


        # Apply the correction to the qubits
        for i in range(len(finalCorrection)):
            pair, qubit = self.qd[i]
            self.q[pair].c[qubit] = int(finalCorrection[i])

        # we return the amount of equivalent corrections, to analyze the performance of the decoder
        return len(bestC)


    def pconf(self, conf):
        '''
        Computes the probability of an error configuration, given in the
        format "01101010001". Useful for the decoder at m=0.
        We assume qubit pairs contain the qubits in order.
        '''
        assert len(conf) == len(self.q) * 2, "mismatch on number of qubits"
        plist = []
        for i in range(len(conf) // 2):
            e0 = int(conf[i * 2])
            e1 = int(conf[i * 2 + 1])
            plist.append( self.q[i].p[e0][e1])
        return np.exp(np.sum(np.log(np.array(plist))))

    def logpconf(self, conf):
        '''
        Computes the log of the probability of an error configuration,
        given in the  format "01101010001". Useful for the decoder at m=0.
        We assume qubit pairs contain the qubits in order.
        '''
        assert len(conf) == len(self.q) * 2, "mismatch on number of qubits"
        p = 0.
        for i in range(len(conf) // 2):
            e0 = int(conf[i * 2])
            e1 = int(conf[i * 2 + 1])
            p += np.log(self.q[i].p[e0][e1])
        return p

    ### Belief Propagation algorithm

    def beliefPropagation(self, niter=3):
        '''
        Belief propagation algorithm.
        '''

        ### 0. Initialization of variables

        nqubits = len(self.qd)  # number of qubits
        nstabs = len(self.stabs)  # number of checks
        bitdegree = 3  # degree of qubits (3 checks)
        # this degree changes from stab to stab (4 or 8)
        #checkdegree = 6  # degree of checks (6 qubits/stabilizer)

        # messages from qubit to stabs (qubitindex,checkindex)
        checkMsg = np.zeros((nqubits, 3))

        # messages from stabilizers to qubits (checkindex,qubitindex)
        bitMsg = np.zeros((nstabs, 8))
        # the size of this will vary from stab to stab, so some of these values will
        # remain at 0 for the red plaquettes with 4 qubits only

        '''
        to match the relations between the second index of both
        matrices (it goes from 0 to the degree-1, instead of indicating
        the index in the full code), we need this 2 dictionaries to 
        get the equivalent link i->j = j->i
        '''
        checktobitMd = {}
        bittocheckMd = {}

        # filling check dictionary
        for i in range(nqubits):
            pair, qubit = self.qd[i]
            slist = self.q[pair].s[qubit]  # list of stabs of this qubit i

            assert len(slist) == bitdegree, "unmatched bitdegree and len(q.s)"

            # we run all stabs j in qubit i
            for j in range(bitdegree):
                '''
                qubit i (0:nqubits) to check j (0,1,2)
                transforms to
                check slist[j] to qubit j (0,1,2,3,4,5)
                '''
                qlist = self.stabs[slist[j]].q


                jb = qlist.index(i)  # index of this qubit i as viewed by stab j

                # every link has an equivalent in the other list
                checktobitMd[(i, j)] = (slist[j], jb)
                bittocheckMd[(slist[j], jb)] = (i, j)

        # Likelihood ratio log((1-p)/p)
        LLR = np.zeros(nqubits)

        for i in range(nqubits):
            pair, qubit = self.qd[i]
            p = self.q[pair].psingle[qubit]

            # we initialize the likelihoood as log((1-p)/p)
            # potential issue: numerical problem here if p<1e-16
            p = self.fixprob(p,1e-15) # short fix
            LLR[i] = self.clipLLR(np.log((1. - p) / p))

            for j in range(bitdegree):
                checkMsg[i, j] = LLR[i]

        ### 1. Belief propagation iterations

        # we repeat as many as niter times
        for it in range(niter):

            ### 1.a Check node update
            for i in range(nstabs):
                checkdegree = len(self.stabs[i].q)
                for j in range(checkdegree):
                    # for every pair check_i, qubit_j:

                    temp = 1.
                    for k in range(checkdegree):
                        if k != j:
                            # we get the indices
                            ic, kc = bittocheckMd[(i, k)]

                            temp *= np.tanh(checkMsg[ic, kc] / 2.)

                    syndrome = self.stabs[i].v

                    # final value of the message

                    bitMsg[i, j] = 2. * np.arctanh(temp) * (1. - 2 * syndrome)

            ### 1.b Bit node update
            for i in range(nqubits):
                for j in range(bitdegree):
                    temp = LLR[i]
                    for k in range(bitdegree):
                        if k != j:
                            # we get the indices
                            ic, kc = checktobitMd[(i, k)]
                            temp += bitMsg[ic, kc]

                    # final value of the message

                    checkMsg[i, j] = temp

        ### 2. Update error probabilities

        newp = np.zeros(nqubits)
        for i in range(nqubits):
            # for every qubit, we add the messages to our likelihood
            temp = LLR[i]
            for j in range(bitdegree):
                # we get the indices
                ic, kc = checktobitMd[(i, j)]
                temp += bitMsg[ic, kc]

            newp[i] = 1. / (1. + np.exp(temp))

        # now we update the probabilities in the code (qubit pairs)
        for i in range(nqubits // 2):
            p0 = self.fixprob( newp[2 * i],self.qubitTolerance)
            p1 = self.fixprob( newp[2 * i + 1], self.qubitTolerance)

            self.q[i].setpsingle(p0, p1)

        return

    def clipLLR(self, p, maxl=10., minl=1e-10):
        '''
        Prevents numerical errors by fixing the maximum and minimum values
        of 'dangerous' functions (in numerical terms)
        '''
        if (np.abs(p) < minl):
            return 0.
        elif (p > maxl):
            return maxl;
        elif (p < -maxl):
            return -maxl;
        else:
            return p;

    ### Update corners functions

    def updatecorners(self, keep=False):
        '''
        Updates the error probabilities of all qubits involved in a
        corner stabilizer according to the syndrome on the corners
        '''
        for s in self.corners:
            if keep:
                self.updatecorner(self.stabs[s])
            else:
                self.updatecornerNoCorrelations(self.stabs[s])
        return

    def updatecorner(self, stab):
        '''
        Updates the error probabilities of the qubits around the stabilizer
        depending on the syndrome. This stabilizer should be in the corner.
        '''
        # first, we identify the qubits involved in the stabilizer
        # and divide them in pairs or single qubits
        qubits = stab.q
        qpairs, singleq = self.identifypairs(qubits)
        v = stab.v

        # we compute the new probabilities for the single qubits
        newpsingle = {}
        for q in singleq:
            # external qubits
            slist = list(singleq)
            slist.remove(q)

            # we obtain details on the qubit we are updating
            pair, ind = self.qd[q]
            pi = self.q[pair].psingle[ind]

            '''
            Now we need to compute the probability of the external qubits
            to be coherent with the syndrome:

            If the value is 0, then we need pe to be the prob of odd #errors
            If the value is 1, then we need pe to be the prob of even #errors

            because we are computing the probability of having already an 
            error in the interior qubit
            '''
            pe = self.pevenodd(qpairs, slist, (v + 1) % 2)

            #newpsingle[q] = pe * pi / (pe * pi + (1. - pe) * (1. - pi))
            newpsingle[q] = self.fixprob( pe * pi / (pe * pi + (1. - pe) * (1. - pi)) ,self.qubitTolerance)

        newppairs = {}
        for pair in qpairs:

            # external qubits
            plist = list(qpairs)
            plist.remove(pair)

            # we need to update all 4 elements of the pair matrix

            # we find the qubit pair from one of the qubits in the pair
            qpairind, ind = self.qd[pair[0]]
            qpair = self.q[qpairind]

            ind1 = [0, 0, 1, 1]
            ind2 = [0, 1, 0, 1]
            newp = [[0., 0.], [0., 0.]]
            oldp = qpair.p
            for i in range(len(ind1)):
                i1 = ind1[i]
                i2 = ind2[i]

                pi = oldp[i1][i2]
                # we compute the even or odd that we need,
                # knowing that s=i+e
                # example: syndrome=1, then for p[0,1] we need even number
                evenorodd = i1 + i2 + v
                # and compute pe so that pi*pe match our need
                # according to the syndrome
                pe = self.pevenodd(plist, singleq, evenorodd % 2)

                #newp[i1][i2] = pe * pi / (pe * pi + (1. - pe) * (1. - pi))
                newp[i1][i2] = self.fixprob( pe * pi / (pe * pi + (1. - pe) * (1. - pi)) ,self.qubitTolerance)

            newppairs[tuple(pair)] = newp

        # now we save the updates
        for pair in qpairs:
            # we find the qubit pair from one of the qubits in the pair
            qpairind, ind = self.qd[pair[0]]
            qpair = self.q[qpairind]

            qpair.setp(newppairs[tuple(pair)])

        for q in singleq:
            # we obtain details on the qubit and update
            pair, ind = self.qd[q]

            pind = self.q[pair].psingle

            pind[ind] = newpsingle[q]

            self.q[pair].setpsingle(pind[0], pind[1])

        return

    def updatecornerNoCorrelations(self, stab):
        '''
        Updates the error probabilities of the qubits around the stabilizer
        depending on the syndrome. This stabilizer should be in the corner.

        Correlations between the qubits in the pairs get lost with
        this corner update, as we break the pairs to update the qubits
        '''
        # first, we identify the qubits involved in the stabilizer
        # and divide them in pairs or single qubits
        qubits = stab.q
        qpairs, singleq = self.identifypairs(qubits)
        v = stab.v

        # we compute the new probabilities for the single qubits
        newpsingle = {}
        for q in singleq:
            # external qubits
            slist = list(singleq)
            slist.remove(q)

            # we obtain details on the qubit we are updating
            pair, ind = self.qd[q]
            pi = self.q[pair].psingle[ind]

            '''
            Now we need to compute the probability of the external qubits
            to be coherent with the syndrome:

            If the value is 0, then we need pe to be the prob of odd #errors
            If the value is 1, then we need pe to be the prob of even #errors

            because we are computing the probability of having already an 
            error in the interior qubit
            '''
            pe = self.pevenodd(qpairs, slist, (v + 1) % 2)

            #newpsingle[q] = pe * pi / (pe * pi + (1. - pe) * (1. - pi))
            newpsingle[q] = self.fixprob( pe * pi / (pe * pi + (1. - pe) * (1. - pi)), self.qubitTolerance)

        newppairs = {}
        for pair in qpairs:
            # external qubits
            plist = list(qpairs)
            plist.remove(pair)

            # we need to update all 4 elements of the pair matrix

            # we find the qubit pair from one of the qubits in the pair
            qpairind, ind = self.qd[pair[0]]
            qpair = self.q[qpairind]

            # we get the qubits in the pair
            q1, q2 = pair

            # we check the single qubit error probabilities
            p1, p2 = qpair.psingle

            # now we compute for qubit 1
            # external single qubits
            slist = list(singleq)
            slist.append(q1)

            pe = self.pevenodd(plist, slist, (v + 1) % 2)
            pi = self.fixprob(p1)
            pe = self.fixprob(pe)
            #potential issue: many numerical errors found here
            #newp1 = pe * pi / (pe * pi + (1. - pe) * (1. - pi))
            newp1 = self.fixprob( pe * pi / (pe * pi + (1. - pe) * (1. - pi)), self.qubitTolerance)

            # now we compute for qubit 2
            # external single qubits
            slist = list(singleq)
            slist.append(q2)

            pe = self.pevenodd(plist, slist, (v + 1) % 2)
            pi = self.fixprob(p2)
            pe = self.fixprob(pe)

            newp2 = self.fixprob( pe * pi / (pe * pi + (1. - pe) * (1. - pi)), self.qubitTolerance)

            # and save the results
            newppairs[tuple(pair)] = [newp1, newp2]

        # now we save the updates
        for pair in qpairs:
            # we find the qubit pair from one of the qubits in the pair
            qpairind, ind = self.qd[pair[0]]
            qpair = self.q[qpairind]

            qpair.setpsingle(*newppairs[tuple(pair)])

        for q in singleq:
            # we obtain details on the qubit and update
            pair, ind = self.qd[q]

            pind = self.q[pair].psingle

            pind[ind] = newpsingle[q]

            self.q[pair].setpsingle(pind[0], pind[1])

        return

    def identifypairs(self, qubits):
        '''
        Given a list of qubits, returns 2 lists, a list of qubit pairs,
        and a list of single qubits. Useful to know when computing
        probabilities.
        '''
        pairs = {}
        qinpairs = {}
        # first, we find out how many pairs we have, and how many qubits of each
        for q in qubits:
            pair, ind = self.qd[q]
            if pair in pairs:
                pairs[pair] += 1
                qinpairs[pair].append(q)
            else:
                pairs[pair] = 1
                qinpairs[pair] = [q]

        # now, we group the qubits in pairs or in single qubits
        qpairs = []
        singleq = []
        for pair in pairs:
            assert pairs[pair] < 3, "more than 2 qubits found in a pair"
            assert pairs[pair] > 0, "pair created with no qubits"
            if pairs[pair] == 2:
                qpairs.append(qinpairs[pair])
            if pairs[pair] == 1:
                assert len(qinpairs[pair]) < 2, "too many qubits for a single"
                singleq.append(qinpairs[pair][0])

        return qpairs, singleq

    def pevenodd(self, qpairs, singleq, evenorodd):
        '''
        Given a list of qubit pairs and single qubits,
        returns the probability of an
        even or odd number of errors happening in the set.
        To compute the value, it finds the qubit pairs and
        calls other functions to compute the total probability
        recursively.
        '''

        assert len(qpairs) + len(singleq) > 0, "There should be at least 1 qubit"

        # first, if there are no pairs we call p single
        if len(qpairs) == 0:
            return self.pevenoddsingle(singleq, evenorodd)

        # if there are single qubits, we take them out
        if len(singleq) > 0:
            # probability of odd in pairs and single qubits
            ppodd = self.pevenodd(qpairs, [], 1)
            psodd = self.pevenoddsingle(singleq, 1)
            if evenorodd == 0:
                # if its even, we need both to be odd or both to b even
                return ppodd * psodd + (1. - ppodd) * (1. - psodd)
            else:
                # if its odd, we need an even+odd combination
                return ppodd * (1. - psodd) + (1. - ppodd) * psodd

        # now, there can only be qubit pairs, so we take them out one at a time
        if len(qpairs) == 1:
            pair, ind = self.qd[qpairs[0][0]]
            pair2, ind2 = self.qd[qpairs[0][1]]
            assert pair == pair2, "Both qubits belong to the same pair"
            return self.pevenoddpair(self.q[pair], evenorodd)

        # at this point, there should only be a group of pairs, so
        # we start computing one at a time
        if len(qpairs) > 1:
            # probability of odd in pairs  qubits
            pair, ind = self.qd[qpairs[0][0]]
            pair2, ind2 = self.qd[qpairs[0][1]]
            assert pair == pair2, "Both qubits belong to the same pair"

            # odd prob of the first pair
            ppodd = self.pevenoddpair(self.q[pair], 1)
            # odd prob of the rest of the pairs
            psodd = self.pevenodd(qpairs[1:], singleq, 1)
            if evenorodd == 0:
                # if its even, we need both to be odd or both to b even
                return ppodd * psodd + (1. - ppodd) * (1. - psodd)
            else:
                # if its odd, we need an even+odd combination
                return ppodd * (1. - psodd) + (1. - ppodd) * psodd

        # if we get to this point, we missed some case
        assert True, "Some case is missing: " + str(len(qpairs)) + ", " + str(len(singleq))

    def pevenoddsingle(self, qubits, evenorodd):
        '''
        Given a list of qubits, this function computes the probability
        of an even/odd number of errors to happen in that list.
        It does not take into account the correlations between qubits
        due to them being in pairs.
        '''
        paux = 1.
        for qubit in qubits:
            pair, q = self.qd[qubit]
            pqubit = self.q[pair].psingle[q]
            paux *= (1. - 2 * pqubit)

        return .5 + (-1) ** evenorodd * .5 * paux

    def pevenoddpair(self, qubitpair, evenorodd):
        '''
        given a qubit pair, returns the probability of an even or an odd
        number of errors in the qubit pair
        '''
        if evenorodd == 0:
            # even number of errors
            return qubitpair.p[0][0] + qubitpair.p[1][1]
        # odd number of errors
        return qubitpair.p[0][1] + qubitpair.p[1][0]

    ### Splitting functions

    def splitinit(self):
        '''
        For every splitting in the code, we need to assign an initial
        probability, based only on the probabilities of the qubits involved
        (for example, if a syndrome is +1, we compute the probability of
        splitting +/+ as the probability of having an even number of errors
        on each side, and -/- as the probability of having an odd number
        of errors)

        Note that for the 488 we are making the approximation of computing
        the probabilities even/odd for each stabilizer in the splitting
        independently, by computing the probability of even/odd number of
        events happening on each half-stabilizer independently.
        To be more precise, we should compute them together and include
        all correlations between the qubits. However, since this is just the
        first estimate of the splittings probabilities, we will keep this
        simplification.
        '''
        for split in self.splits:
            # check the even/odd probabilities at both sides of the splitting

            for choice in range(4):

                # for each choice, we need to go through the 4 half stabilizers
                # and compute the probability of an even/odd number of errors in
                # the qubits that belong to the support of that half stabilizer
                # Even if the half stabilizer has parity 0
                # Odd  if the half stabilizer has parity 1
                pchoice= 1
                for stabInd in range(2):
                    for cellInd in range(2):

                        # qubits on each cell, for each stabilizer
                        qlist = split.getSupport(stabInternalIndex=stabInd, cellInternalIndex=cellInd)
                        # get the qubit pairs for which these qubits belong to
                        qpairs, qs = self.identifypairs(qlist)

                        # get parity of the half stabilizers for each choice
                        parity = split.getvalue(choice=choice, cellInternalIndex=cellInd)[stabInd]

                        # compute probability of even/odd for each stabilizer
                        pchoice *= self.pevenodd(qpairs,qs, parity)
                # product of all of them
                split.psplit[choice] = self.fixprob(pchoice,self.tolerance)
            # renormalize
            split.psplit /= np.sum(split.psplit)
        return

    def splitupdates(self, nsteps=10, nmean=1, geometricAverage = True):
        '''
         We apply the soft splitting method to update the splitting probabilities.
        The method can be extended to apply the mean value of last 2 steps.

        nmean: number of updates between mean value steps
        nsteps: number of cycles of nmean steps
        '''
        pnew = np.zeros((len(self.splits),4))
        ###############
        # case nsteps=0
        #############
        if nsteps == 0:

            # case nsteps=0
            for j in range(nmean):

                for k in range(len(self.splits)):
                    pnew[k] = self.updatesplit(self.splits[k])

                # no meanvalue step
                for k in range(len(self.splits)):
                    self.splits[k].psplit = pnew[k]
                self.updatesplitchoice()

            return self.updatesplitchoice()
            ###############
        # case nsteps>0
        ################
        for i in range(nsteps):
            # updates of the splittings
            for j in range(nmean):
                # first, we update the splittings with the new value
                if j > 0:
                    for k in range(len(self.splits)):
                        self.splits[k].psplit = pnew[k]

                for k in range(len(self.splits)):
                    pnew[k] = self.updatesplit(self.splits[k])

            # meanvalue step every nmean iterations
            for k in range(len(self.splits)):

                if geometricAverage:
                    # geometric mean
                    self.splits[k].psplit = np.sqrt(np.array(pnew[k]) * np.array(self.splits[k].psplit))
                    # we need to renormalize the probabilities
                    self.splits[k].psplit /= np.sum(self.splits[k].psplit)
                else:
                    # arithmetic mean
                    self.splits[k].psplit=(pnew[k]+self.splits[k].psplit)/2.
                    # we renormalize the probabilities, although shouldn't be necessary
                    self.splits[k].psplit /= np.sum(self.splits[k].psplit)

            self.updatesplitchoice()

        return self.updatesplitchoice()

    def updatesplitchoice(self):
        '''
        Updates the choice of splitting based on the probability of splitting
        Picks the best choice for the splitting given the estimates of the split probabilities
        If there are several choices with almost equal probability, picks at random
        '''
        # now we update the choice of the splitting
        dp = 0.0001  # margin to choose at random

        for split in self.splits:
            bestchoice = []
            bestp = -90

            for choice in range(4):
                split.psplit[choice]= self.fixprob(split.psplit[choice])
                pchoice = split.psplit[choice]
                if pchoice>bestp-dp:

                    # if it is really close to the previous best, we store both
                    if pchoice<bestp+dp:
                        bestchoice.append(choice)
                    else:
                        # otherwise, we update our best choice
                        bestp = pchoice
                        bestchoice = [choice]

            # after going through all 4 choices, we pick the best

            if len(bestchoice)==0:
                print("Problem with the split choice probabilities: ", split.psplit)

            if len(bestchoice)<2:
                split.choice = bestchoice[0]
            else:
                # if there are several close choices, we pick at random
                split.choice = bestchoice[np.random.randint(len(bestchoice))]


        return

    def updatesplit(self, split):
        '''
        This function updates the probability of a split choice using the
        functions defined in the cells
        Returns an array with the 4 probabilities
        '''

        # we identify the cells involved
        cell0 = split.cells[0]
        cell1 = split.cells[1]

        # we need an estimate of the probability for each of the split choices
        pnew = np.zeros(4)

        pu = np.zeros(4)
        pl = np.zeros(4)
        for choice in range(4):

            if self.hardSplitting:
                pu[choice] = cell0.phardSplits(split, choice)
                pl[choice] = cell1.phardSplits(split, choice)
            else:
                # now we compute each of the probabilities ps^u and ps^l
                pu[choice] = cell0.psoft(split, choice)
                pl[choice] = cell1.psoft(split, choice)
            if not self.useposterior:
                pnew[choice] = self.fixprob(pu[choice] * pl[choice])

        # if we are using the posterior distribution, we need to renormalize psoft
        if self.useposterior:
            pu = pu/np.sum(pu)
            pl = pl/np.sum(pl)
            for choice in range(4):
                pnew[choice] = self.fixprob(pu[choice] * pl[choice])


        # the normalization factor is the sum of all of these 4 choices
        pnew = pnew/sum(pnew)

        # now we apply the fixp with the tolerance for the splits
        # to fix the minimum probability for a split
        for choice in range(4):
            pnew[choice] = self.fixprob(pnew[choice], self.splitTolerance)

        return pnew/sum(pnew)

    ### Single Cell Operations and Rescaling

    def decodecells(self):
        '''
        This method finds the most probable correction for every cell, given the splittings
        found in the split updates.

        #potential alternative (not included): choose the most probable error class
        '''

        for cell in self.cells:
            # for each cell, we find the resulting syndrome after splitting
            syndrome = cell.currentSyndrome()

            # then we find the configurations for that syndrome
            confs = cell.lookuptable(syndrome)

            bestp = -6.
            # we find the best configuration
            for conf in confs:
                p = cell.pconf(conf)
                if p > bestp:
                    bestp = p
                    bestc = conf
            # this method only finds the first one, there could
            # be more confs with the same probability that we ignore now
            # inefficient
            # now we apply corrections into the qubit pairs
            for i in range(len(bestc)):
                if bestc[i] == "1":
                    qp, qi, qm = cell.qd[i]
                    cell.qp[qp].c[qi] = 1


        return

    def rescaling(self):
        '''
        This method computes the error probability of the rescaled cell,
        applying a soft-rescaling procedure in which the different
        combinations of splittings are considered according to their prob.
        '''

        for i in range(len(self.cells)):
            pmatrix = self.rescalecell(self.cells[i])
            # compute probabilities
            # self.cells[i]---> rescaling probabilities
            self.newcode.q[i].setp(pmatrix)
        self.syndrome()
        # that syndrome needs to be added
        for i in range(len(self.newcode.stabs)):
            self.newcode.stabs[i].v = self.stabs[self.corners[i]].v
            self.newcode.stabs[i].vmeasured = self.stabs[self.corners[i]].v

    def rescalecell(self, cell):
        '''
        This method computes the matrix of probabilities of the rescaled
        cell by using the soft rescaling method.
        '''

        pass  # to be defined in the particular lattice

    def backscale(self):
        '''
        This method translates the corrections applied at the next
        level of rescaling to this level of rescaling, using the correction
        applied on the next level to correct the correction at the current one
        '''

        for i in range(len(self.newcode.q)):
            '''
            Every qubit pair in the newcode should correspond to a cell in 
            the current code, so qubit pair i corresponds to cell i.
            '''

            for ind in range(len(self.newcode.q[i].c)):

                # we apply the logical operator if there is a correction
                if self.newcode.q[i].c[ind] == 1:
                    self.cells[i].applylogicop(ind)
        return

    ### Logical check and simulations

    def check(self):
        '''
        This method checks the logical operators after applying the
        error and the correction, to see if there is any logical error.
        '''
        total = 0
        partial = []
        for op in self.logicops:

            # for every operator, we check the errors in its qubits
            part = 0
            for q in op:
                qpair, qind = self.qd[q]
                part += self.q[qpair].e[qind]
                part += self.q[qpair].c[qind]
            part = part % 2

            total += part
            partial.append(part)

        total = min(total, 1)
        return total, np.array(partial)

    '''
    Other functions
    '''

    def simulate(self, N=100, p=0.01,cornerupdates = True, nsteps=1, nmean=5, BPsteps=3, renormalizeOn = False,
                 fixp = -5):
        '''
        This function simulates N error configurations for
        a given error probability p and applies the decoder.
        The result is the average of all runs.
        '''
        Any=0
        AllOps = np.zeros(4)
        for i in range(N):
            any, allOps = self.simulation(p,cornerupdates, nsteps, nmean, BPsteps,renormalizeOn,fixp)
            Any+=any
            AllOps+=allOps
        return Any,AllOps

    def simulation(self, p=0.01,cornerupdates = True, nsteps=1, nmean=5, BPsteps=3,
                   renormalizeOn = False, fixp=-5,debugText = False):
        '''
        This function applies the decoder to a randomly
        generated error configuration
        '''
        self.clear(p)
        self.noise()
        self.syndrome()
        return self.decoder(cornerupdates,nsteps, nmean, BPsteps,renormalizeOn,fixp = fixp,debugText= debugText)
        #return self.check()

    def clear(self, p=0.01):
        '''
        This function resets the values of all variables
        in the code, and sets the error probability to p
        '''
        # for the qubits:
        for pair in self.q:
            # we reset the error probabilities
            pair.setpsingle(p, p)
            # we reset the correction and the error to 0
            pair.e = [0, 0]
            pair.c = [0, 0]
        # for the stabilizers
        for s in self.stabs:
            s.vmeasured = 0
            s.v = 0
        # for the subcode:
        if self.m > 0:
            self.newcode.clear()

    def noise(self):
        '''
        noise generation for qubit pairs.
        We generate a random number for each pair and decide
        the error configuration according to the matrix of
        probabilities.
        '''
        for pair in self.q:
            r = np.random.random()
            p = 0.
            for e1 in range(2):
                for e2 in range(2):
                    # we add the error probability of that configuration
                    p += pair.p[e1][e2]
                    if r <= p:
                        pair.e = [e1, e2]
                        r = 32
                        # non elegant way to get out of the loop

    def syndrome(self):
        '''
        This function runs through the stabilizers and measures them
        '''

        for s in self.stabs:
            v = 0
            c = 0
            for q in s.q:
                # check error
                pair, qubit = self.qd[q]
                if self.q[pair].e[qubit] == 1:
                    v += 1
                if self.q[pair].c[qubit] == 1:
                    c += 1
            s.vmeasured = max(s.vmeasured, v % 2)
            s.v = (s.vmeasured + c) % 2

        return

    def checksyndrome(self):
        '''
        after the correction, checks that the correction brings the code back
        to codespace
        '''
        self.syndrome()
        sums = 0
        for stab in self.stabs:
            sums += stab.v
        assert sums == 0, str(sums) + " some stabilizers have not been corrected" + str(self.m)
        return sums

    def fixprob(self, p,tolerance = -1):
        '''
        This function fix probabilities under a certain threshold, so
        that there is no problem with probabilities being zero
        '''
        '''
        newp=max(p,1e-15)#we fix a minimum value for p
        newp=min(newp,1-1e-15)#and a maximum value for p (never exactly one)
        return newp
        This function is already defined in the cells, so we avoid duplicates:
        '''
        if tolerance<0:
            tolerance = self.tolerance

        return self.cells[0].fixprob(p,tolerance)

    def renormalizeP(self,p):
        '''
        Applies a transformation to the probability, that maps the interval [1e-15, 0.5] to [1e-5, 0.5],
        by essentially taking the cubic root. Slight modification to keep 1/2 as the fixed point of the transformation
        This transformation is monotonic, so the ordering of the probabilities stays the same
        '''
        return (2 * p) ** (1 / 3) / 2

    def renormalizeProbabilities(self):
        '''
        Goes through all qubits in the lattice and applies the renormalization mapping to the probabilities of the
        qubits. It removes all correlations in the process.
        The goal is to avoid numerical issues due to probabilities being too small for the numerical precission.
        '''

        for qpair in self.q:
            p0,p1 = qpair.psingle
            newp0 = self.renormalizeP(p0)
            newp1 = self.renormalizeP(p1)
            qpair.setpsingle(newp0,newp1)
        return


    def pcorrection(self):
        '''
        This function computes the probability of the current correction
        according to the probabilities stored in the qubits
        '''
        p = 1.
        for q in self.q:
            p *= q.p[q.c[0]][q.c[1]]
        return p


    def setError(self,errorList):
        '''
        Sets the qubits in errorList as errors in the code, and measures the syndrome
        :param errorList: list of qubits with errors, e.g. [0,2,4,5]
        '''
        for q in errorList:
            qpair, qind = self.qd[q]
            self.q[qpair].e[qind] = 1
        self.syndrome()
        return

    def saveError(self):
        '''
        Returns a list with the positions of the errors, readable by setError()

        :return: list of error positions e.g. [1,42,27]
        '''
        errorList = []
        for q in range(self.Nq):
            qpair, qind = self.qd[q]
            if self.q[qpair].e[qind] == 1:
                errorList.append(q)
        return errorList



    def countErrors(self):
        '''
        Counts the number of errors and corrections in the code
        :return: #errors, #corrections
        '''
        errors = 0
        corrections = 0
        for i in range(self.Nq):
            qpair, qind = self.qd[i]
            errors += self.q[qpair].e[qind]
            corrections += self.q[qpair].c[qind]
        return errors,corrections

    def setCutoff(self,cutoff, qubitCutoff = -1,splitCutoff = -1):
        '''
        Sets both the tolerance and the qubit tolerance to the cutoff value
        :param cutoff:
        '''
        self.tolerance = cutoff
        if qubitCutoff>0:
            self.qubitTolerance = qubitCutoff
        else:
            self.qubitTolerance = cutoff

        if splitCutoff > 0:
            self.splitTolerance = splitCutoff
        else:
            self.splitTolerance = cutoff

        if self.m>0:
            self.newcode.setCutoff(cutoff, qubitCutoff, splitCutoff)
            for cell in self.cells:
                cell.tol = cutoff