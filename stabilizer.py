
"""
Color code decoder: stabilizer and splitting class definition

################
Stabilizer class
    contains information about a given stabilizer in the code:
    i: index of the stabilizer in the code
    q: list of qubits in the stabilizer e.g. q=[0,1,2,3,4,5]
    vmeasured: value of the parity of the stabilizer from the measurement info
    v: parity of the stabilizer after the corrections

###############
Splitting class
    contains information about a splitting, which contains two stabilizers

    s: stabilizers involved in the splitting, using the stab class e.g. [Stab(1),Stab(2)]
    cells: cells involved in the splitting

@author: pedroparrado
"""

import numpy as np

class Stabilizer:
    
    '''
    This class contains information about a stabilizer in the code:
    the support of the stabilizer and its value
    '''
        
    def __init__(self,i=0):
        # q is the list of indexes of the qubits that apply to the stabilizer
        self.q=[]
        # v: value of the stabilizer: 0 means +1 measured, 1 means -1 (excited)
        # vmeasured does not include changes due to corrections, v does
        self.vmeasured=0
        self.v=0
        #index in the full code
        self.index=i

class Splitting:
    '''
    This class contains information about a splitting in the code.
    To init, introduce the stabilizer it splits, and the cells involved
    as [cell0,cell1]
    '''
    def __init__(self,s,cells=[],ind=0):
        '''
        Initializes the splitting structure
        :param s: stabilizers involved in the splitting
        :param cells: cells involved  in the splitting
        :param ind: index of the splitting in the code
        '''
        #stabilizers to which it applies. it has to be a Stabilizer class
        self.s=s

        # global indices of the stabilizers in the splitting
        self.sindex = (self.s[0].index,self.s[1].index)
        # dictionary sd[global index] = internal index (0 or 1)
        self.sd = {}
        self.sd[self.s[0].index] = 0
        self.sd[self.s[1].index] = 1
        
        #indices of the qubits (in the main code) split in cells
        self.q=[[],[]]
        
        
        #cells involved
        self.cells=cells

        #dictionary with the indices: given the index of a cell, return 0 or 1
        self.celld={}
        if len(cells)>1:
            self.celld[cells[0].i]=0
            self.celld[cells[1].i]=1
        
        
        # current choice e.g. 0  ++, 1 -+; 2 +- and 3 --
        self.choice=0
        
        #matrix containing all possible  values according to:
        #value of the stabilizer, value of the choice, cell to which it applies
        #self.spt=[[[0,0],[1,1]],[[1,0],[0,1]]]
        # splitStabValue [firstStabValue e.g. 0/1][second StabValue e.g. 0/1] [splitchoice :0-3] [cell: 0-1]
        self.splitStabValue = [
            # first stabilizer value +
            [
                # second stabilizer value +
                [
                    # split choice 0: ++,++
                    [ (0,0) , (0,0)   ], # first cell, second cell: (value of stab 1, value of stab 2)
                    # split choice 1: -+,-+
                    [ (1,0) , (1,0)   ], # first cell, second cell: (value of stab 1, value of stab 2)
                    # split choice 2: +-,+-
                    [ (0,1) , (0,1)   ], # first cell, second cell: (value of stab 1, value of stab 2)
                    # split choice 3: --,--
                    [ (1,1) , (1,1)   ], # first cell, second cell: (value of stab 1, value of stab 2)
                ],

                # first stabilizer value +
                # second stabilizer value -
                [
                    # split choice 0: +-,++
                    [ (0,1) , (0,0)   ], # first cell, second cell: (value of stab 1, value of stab 2)
                    # split choice 1: --,-+
                    [ (1,1) , (1,0)   ], # first cell, second cell: (value of stab 1, value of stab 2)
                    # split choice 2: ++,+-
                    [ (0,0) , (0,1)   ], # first cell, second cell: (value of stab 1, value of stab 2)
                    # split choice 3: -+,--
                    [ (1,0) , (1,1)   ], # first cell, second cell: (value of stab 1, value of stab 2)
                ]
            ],
            # first stabilizer value -
            [
                # second stabilizer value +
                [
                    # split choice 0: -+,++
                    [(1, 0), (0, 0)],  # first cell, second cell: (value of stab 1, value of stab 2)
                    # split choice 1: ++,-+
                    [(0, 0), (1, 0)],  # first cell, second cell: (value of stab 1, value of stab 2)
                    # split choice 2: --,+-
                    [(1, 1), (0, 1)],  # first cell, second cell: (value of stab 1, value of stab 2)
                    # split choice 3: +-,--
                    [(0, 1), (1, 1)],  # first cell, second cell: (value of stab 1, value of stab 2)
                ],
                # first stabilizer value -
                # second stabilizer value -
                [
                    # split choice 0: --,++
                    [(1, 1), (0, 0)],  # first cell, second cell: (value of stab 1, value of stab 2)
                    # split choice 1: +-,-+
                    [(0, 1), (1, 0)],  # first cell, second cell: (value of stab 1, value of stab 2)
                    # split choice 2: -+,+-
                    [(1, 0), (0, 1)],  # first cell, second cell: (value of stab 1, value of stab 2)
                    # split choice 3: ++,--
                    [(0, 0), (1, 1)],  # first cell, second cell: (value of stab 1, value of stab 2)
                ]
            ]
        ]

        '''
        example:
            stabilizer measurements=0,1
            choice=2
            
                spt[0][1][2]=[(0,0) , (0,1) ] #first the two results for the first cell, then for the second cell
            if we specify the cell 0 or 1:
                    spt[0][1][2][0]=(0,0)
                    spt[0][1][2][1]=(0,1) 
        or you can use the built in function getp
        '''
        #probability of each splitting choice
        self.psplit=np.array([.25,.25,.25,.25])
        
        #index in the full code
        self.index=ind

        # support of each stab on each cell (qubits to which it applies)
        # [ [ [1,2,3],[2,3,4] ],[ [5,6,7],[6,7,8] ] ]
        self.support = None
        self.supportIsNotInitialized = True
    
    def initcells(self,cells):
        
        #cells involved
        self.cells=cells
        #dictionary with the indices: given the index of a cell, return 0 or 1
        self.celld={}
        if len(cells)>1:
            self.celld[cells[0].i]=0
            self.celld[cells[1].i]=1




    def initSupport(self):
        '''
        Initializes the internal lists with the support of each stabilizer on each of the cells
        '''

        # support[stab internal index][cell internal index] = [q1,q2,...] # list of qubits on the support
        self.support = [[[],[]],[[],[]]]

        for stab in range(2):
            for qubitInd in self.s[stab].q:     # this gives us the list of indices of qubits in stab

                if qubitInd in self.cells[0].qout:
                    self.support[stab][0].append(qubitInd)
                elif qubitInd in self.cells[1].qout:
                    self.support[stab][1].append(qubitInd)
                else:
                    assert True , "Qubit "+str(qubitInd)+" was not in any of the cells of split "+str(self.index)

        self.supportIsNotInitialized = False
        return # after this function, support[stabindex][cellindex] is a list of the qubits on the support of stab

    def getSupport(self,stabGlobalIndex = -1, stabInternalIndex = -1,
                   cellGlobalIndex = -1,cellInternalIndex = -1,neighbor = False ):
        '''
        Returns a list with the qubits of the support of a given stabilizer on a given cell.
        If neighbor=True, then it returns the support of the neighboring stabilizer
            (that is useful for the soft rescaling rule)
        The input for the stab index and the cell index can be given both with the internal index 0,1 of this stabilizer
        or with the global index of the stab/cell in the code
        :param stabGlobalIndex:
        :param stabInternalIndex:
        :param cellGlobalIndex:
        :param cellInternalIndex:
        :param neighbor:
        :return:
        '''

        # if the support has not been initialized yet, we do it now
        if self.supportIsNotInitialized:
            self.initSupport()

        # we process the input for the stabilizer
        if stabInternalIndex>=0:
            stabInd = stabInternalIndex
        elif stabGlobalIndex>=0:
            stabInd = self.sd[stabGlobalIndex]

        # we process the input for the cell
        if cellInternalIndex>=0:
            cellInd = cellInternalIndex
        elif cellGlobalIndex>=0:
            cellInd = self.celld[cellGlobalIndex]

        if neighbor:
            stabInd = (stabInd+1)%2

        return self.support[stabInd][cellInd]


    
    #just an easier way to get the value of the half splitting
    def getvalue(self,choice=-1,stabilizerOrder = None,cellInternalIndex=-1,cellGlobalIndex=-1, measuredValue=False):
        '''
        Given a choice of splitting and the index of a cell within the splitting, returns the value of the
        sign of the two splittings
        :param choice: splitting choice: 0-3
        :param stabilizerOrder: global indices of the stabilizer
        :param cellInternalIndex:  index of the cell within the split: 0-1
        :param cellGlobalIndex:  index of the cell within the code: 0-ncells
        :param measuredValue: utilizes the measured sign instead of the modified sign after correction

        :return: tuple of values of the split signs e.g. (0,1) meaning (+,-) in first and second stabs
        '''

        if stabilizerOrder is None:
            stabilizerOrder = self.sindex
        # with this we make sure that we interpret correctly the order of the stabilizers
        order0 = self.sd[stabilizerOrder[0]]
        order1 = self.sd[stabilizerOrder[1]]

        #choice and cell should be either 0 or 1, else it uses the current
        if choice==-1:
            choice=self.choice

        if cellInternalIndex>=0:
            celli = cellInternalIndex
        elif cellGlobalIndex>=0:
            celli = self.celld[cellGlobalIndex]
        else:
            assert True,  "No input cell received"

        if measuredValue:
            tupValues= self.splitStabValue[self.s[0].vmeasured][self.s[1].vmeasured][choice][celli]
            #[self.celld[celli]]
        else:
            tupValues= self.splitStabValue[self.s[0].v][self.s[1].v][choice][celli]
            #[self.celld[celli]]

        # we make sure that we return them in the order of stabilizerOrder
        return (tupValues[order0],tupValues[order1])

    # an easy way to access the probability of a certain half value:
    def getp(self, stabilizerOrder=None, cellInternalIndex=-1, cellGlobalIndex=-1, valuesInput=(0, 0)):
        '''
        introducing the index of the cell and the values of its
        half stabilizers, we get the probability of those half splitting values
        :param stabilizerOrder: tuple of indices of the stabilizers in the same order as the input values
        :param cellInternalIndex:  index of the cell within the split: 0-1
        :param cellGlobalIndex:  index of the cell within the code: 0-ncells
        :param valuesInput: tuple of values of the half-stabilizers

        :return probability of the input choice of splitting values
        '''
        p = {}

        if stabilizerOrder is None:
            stabilizerOrder = self.sindex
        # with this we make sure that we interpret correctly the order of the stabilizers
        order0 = self.sd[stabilizerOrder[0]]
        order1 = self.sd[stabilizerOrder[1]]
        values = (valuesInput[order0], valuesInput[order1])

        if cellInternalIndex >= 0:
            celli = cellInternalIndex
        elif cellGlobalIndex >= 0:
            celli = self.celld[cellGlobalIndex]
        else:
            assert True, "No input cell received"

        for choice in range(4):
            p[self.splitStabValue[self.s[0].v][self.s[1].v][choice][celli]] = self.psplit[choice]
        return p[values]

    
    