
In this document, we go through the main functions needed for some
of the calculations in the rescaling decoder.

With this document, the relation between functions in the code
and equations in the algorithm should be more clear.



Functions for the splitting algorithm:

    # Finding all error configurations compatible with a syndrome
    cell.lookuptable(syndrome)
        # given a syndrome in list form, e.g. [0,0,1,0,1...]
        # returns a list with all possible corrections in
        # a string format, e.g.:
        # ['000101000','01001010',..]
        # where each 0/1 refers to the qubits in the cell

    # Probability of a given error configuration
    cell.pconf(configuration)

        # p(error configuration) = prod (p_i) prod(1-p_j)

        # given a configuration in string format '01000101'
        # it computes the probability of the configuration
        # as the product of the probabilities of the qubits
        # using the correlations from the Qubitpairs
        # Note: it uses logarithms to prevent numerical errors


    # Probability of all error conf. given a splitting
    # p(e | syndrome) = sum( p(error configuration)
    cell.pall(syndrome)
        # computes the sum of the probabilities of all
        # error configurations compatible with the given syndrome

    # Probability of a split given the rest of splittings
    # p(s0s1| s2s3s4...)  =     p(e|syndrome)
                sum_choices ( p(e| s0's1'+syndrome) )
    cell.psingle(syndrome,splitIndex)
        # computes the probability of a splitting choice, given
        # the rest of the splitting choices, by taking
        # p(e|syndrome)
        # and dividing it by the sum of all possible split choices
        # for the splitting "index", e.g. for index=0 :
        p(e|++,s2s3s4..)+p(e|+-,s2s3s4..)+p(e|-+,s2s3s4..)+p(e|--,s2s3s4..)

    # Probability of a split choice given the information from a single cell
    # p(s0s1 ^cell1) = sum( p(s0s1|s2s3s4s5...)* p(s2s3) * p(s4s5) *...)
    cell.psoft(split,choice)
        # computes the probability of a splitting choice within a cell,
        # by taking into account all possible splitting configurations
        # and their estimates
        # Input: split as the Splitting item in the code
                choice as integer
        # returns the probability

    # Auxiliar function used in psoft to compute the product of the priors
    # for all the splitting configurations
    cell.confGenerator(splitInd,choice)
        # generates all the splitting configurations for the rest of the
        # splittings in the cell, and computes their probability


    # Puts together the estimates of the probability of a splitting
    # from the two neighboring cells
    colorcode.updatesplit(split)
        # split is an Splitting item
        # combines the information from cell.psoft() on both cells,
        # for all 4 splitting choices, and returns an array with
        # all 4 probabilities.




Functions for the rescaling algorithm:

    cell.splitconfGenerator()
        # computes all possible splitting choice permutations, together with their
        # probability and the support of the neighbors of the changed splittings
        # returns qconfs,pconfs
        # qconfs: list with the support of the neighbor of the changed splittings
        # pconfs: list with the splitting probability of that choice of splitting

    lattice488.rescalecell(cell)
        # computes all the matrix of rescaling probabilities for the cell
        # [id, L0, L1, L0L1]
        # by combining all permutations of bulk stabilizers and logical operators
        # for each choice of splittings. For the equations check the notes