
"""
Created on  Feb  2021

Color code decoder: cell definition

@author: pedroparrado
"""
import numpy as np

class Cell:
    
    '''
    This class contains information about a cell in the code. This information
    includes the indexes of the qubits, stabilizers and splittings involved 
    in the cell, the indexes of the neighboring cells and a lookuptable.
    
    The class also contains methods useful for the decoder.
    '''
        
    def __init__(self,ind, shortcuts = False):
        #index of the cell in the main code
        self.i=ind
        
        # list of qubit pairs in the cell
        self.qp=[]


        '''  qd[qubit index in cell] = (quitpair index in cell, qubit index in pair, qubitpair index in fullCode) 
        qd is a dictionary made into the same fashion as the main
        code: qd[3] will give you a tuple (1,0,-1) meaning that
        the qubit 3 in qd is in the qubitpair 1 in qp, and
        inside that pair qd[3] is the qubit 0. The third index
        refers to the index of the pair qubit in the full code        
        '''
        self.qd={}
        # qd[index in the cell] = (qubit pair, index within the qubit pair, index of the qubit pair in the main code)
        
        '''
        #qout is another dictionary. the key is the index in the
        #main code, the value the index here (key for qd)      
        '''
        self.qout={}
        # qout[index of qubit in main code] = index of qubit in the cell
        
        #splits is the list of the splittings in the boundaries
        self.splits=[]
        # contains references to the Splitting items


        # stab is the list of the stabilizers that apply to the qubits in
        # the cell excluding the corners (boundaries and bulk) for the purpose
        # of generating the lookuptable
        self.stab=[]

        #lookuptable variable
        self.lookuptabledata=[]
        
        #logical operators
        '''
        each logical operator contains the indices of the qubits 
        you need to change. For example, for a 4 qubit cell would
        be 0,2,3, ommiting the qubit in the center of the triangle.
        With this index you can check the qubit pair using self.qd
        '''
        self.logicops=[]

        # variable to decide if we compute only the configuration with highest probability
        self.prunningShortcuts = shortcuts

        # tolerance of the fixed probability
        self.tol = 1e-20
        self.useposterior = False


        
    
    def lookuptable(self,stabs):
        '''
        This function returns a list of all possible configurations
        of errors compatible with the syndrome given by stabs

        # given a syndrome in list form [0,0,1,0,1...]
        # returns a list with all possible corrections in
        # a string format
        # ['000101000','01001010',..]
        # where each 0/1 refers to the qubits in the cell
        '''
        synb=''

        for stab in stabs:
            synb+=str(int(stab))
        index=int(synb,2)

        return self.lookuptabledata[index]

    def pconfLogarithms(self, conf):
        '''
        # p(error configuration) = prod (p_i) prod(1-p_j)

        # given a configuration in string format '01000101'
        # it computes the probability of the configuration
        # as the product of the probabilities of the qubits
        # using the correlations from the Qubitpairs
        # Note: it uses logarithms to prevent numerical errors
        '''
        plist = []
        # since in the 488 all qubits of a qubit pair are in the cell
        # we only need to loop over the qubit pairs of the cell

        for i in range(len(self.qp)):
            qpair = self.qp[i]
            # all qubits in the qubit pairs should be in the correct order
            v0 = int(conf[2 * i]) % 2
            v1 = int(conf[2 * i + 1]) % 2
            #plist.append(self.fixprob(qpair.p[v0][v1]))
            plist.append(qpair.p[v0][v1])

        # we use logarithms to avoid numerical problems with small numbers
        p = np.exp(np.sum(np.log(np.array(plist))))

        return p
        #return self.fixprob(p)

    def pconf(self, conf):
        '''
        # p(error configuration) = prod (p_i) prod(1-p_j)

        # given a configuration in string format '01000101'
        # it computes the probability of the configuration
        # as the product of the probabilities of the qubits
        # using the correlations from the Qubitpairs
        '''
        p = 1
        # since in the 488 all qubits of a qubit pair are in the cell
        # we only need to loop over the qubit pairs of the cell

        for i in range(len(self.qp)):
            qpair = self.qp[i]
            # all qubits in the qubit pairs should be in the correct order
            v0 = int(conf[2 * i]) % 2
            v1 = int(conf[2 * i + 1]) % 2
            p*= qpair.p[v0][v1]
        return p
        #return self.fixprob(p)

    def pall(self,stabs):
        '''
        this function returns the probability p(e|s0,s1,s2,...), by adding
        the probabilities of all possible error configurations 
        compatible with the syndrome s0,s1,s2,... given in stabs

        '''
        #first step is to obtain the configurations from the lookuptable
        configurations=self.lookuptable(stabs)

        # shortcut, compute only the probability of the most probable configuration ( the first)
        if self.prunningShortcuts:
            return self.fixprob(self.pconf(configurations[0]))
            #return self.pconf(configurations[0])



        #second step is to compute the probability of those configurations
        #and add all of them
        ptot=0.0
        for conf in configurations:
            '''
            To compute the probability of a configuration of errors,
            we need to multiply a factor of p for every error and q for
            every qubit without error. However, every qubit is part 
            of a pair of qubit, and the error probabilities are correlated,
            so we need to get the probability of error from the matrix
            '''
            p=self.pconf(conf)           
            ptot+=p

        return self.fixprob(ptot)
    
    
    
    def psingle(self,stabs,index):
        '''
        This function computes p(s_index|stabs)
        stabs: syndrome,  index: splitting index in the cell
        # computes the probability of a splitting choice, given
        # the rest of the splitting choices, by taking
        # p(e|syndrome)
        # and dividing it by the sum of all possible split choices
        # for the splitting "index", e.g. for index=0 :
        p(e|++,s2s3s4..)+p(e|+-,s2s3s4..)+p(e|-+,s2s3s4..)+p(e|--,s2s3s4..)

        # p(s0s1| s2s3s4...)  =     p(e|syndrome)
                    sum_choices ( p(e| s0's1'+syndrome) )
        '''
        pchoice = self.pall(stabs)

        pallChoices = 0.0
        choices = [(0,0),(0,1),(1,0),(1,1)]

        for choice in choices:
            changedstabs = list(stabs)
            changedstabs[2*index]   = (stabs[2*index]   + choice[0]) % 2
            changedstabs[2*index+1] = (stabs[2*index+1] + choice[1]) % 2

            pallChoices += self.pall(changedstabs)

        pallChoices = self.fixprob(pallChoices)

        ''' # important patch note:
        If the sum of probabilities of all choices is on the order of magnitude of the tolerance,
        that means that the probability of that split configuration was small to begin with:
        p(s_index|S) * p(S) <--- this prior
        that prior p(S) is obtained from the estimates of the split choice probabilities.
        However, if we find at this point that all split choices for that configuration are bad 
        (i.e. all smaller than the tolerance value) then we are getting new information about 
        that p(S), so we can multiply that prior with a small number.
        For simplicity of the code, we add that factor right here by assigning tol to this output
        '''
        if self.useposterior and pallChoices<5*self.tol:
            return self.tol

        # otherwise, usual normalization
        return self.fixprob(pchoice/pallChoices)
    
    
    def psoft(self,split,choice):
        '''
        Computes p(s^u_0, s^u_1) by adding all configurations of split choices for the other splits

        :param split: split item for the splitting pair
        :param choice: integer for the current split choice
        '''

        #we get the index of the splitting
        splitIndex=self.splits.index(split)

        #we compute the different configurations of splittings
        configs,psplit=self.confGenerator(splitIndex,choice)

        totalp = 0.0
        for i in range(len(configs)):
            thisConf = configs[i]
            thisPSplit = psplit[i]
            newp = self.psingle(thisConf, splitIndex)

            # we multiply the conditional probability and the prior of the splitting probabilities
            # p(s0,s1 | S) * p(S)
            totalp += newp * thisPSplit


        return self.fixprob(totalp)

    def phardSplits(self,split,choice):
        '''
        Computes p(s^u_0, s^u_1) by adding only the current configuration of split choices for the other splits

        :param split: split item for the splitting pair
        :param choice: integer for the current split choice
        '''

        #we get the index of the splitting
        splitInd=self.splits.index(split)

        #we compute the different configurations of splittings
        config = self.currentSyndrome()


        stabOrder = (self.stab[2*splitInd].index, self.stab[2*splitInd+1].index)
        v0,v1 = self.splits[splitInd].getvalue(choice = choice,
                                               stabilizerOrder=stabOrder,
                                               cellGlobalIndex=self.i)

        config[2*splitInd]   = v0
        config[2*splitInd+1] = v1

        totalp = self.psingle(config,splitInd)

        return self.fixprob(totalp)


    def confGenerator(self,splitInd,choice):
        '''
        Generates all configurations of split values fixing ind at value value
        '''
        
        #we will only need to accound the configurations for the split
        #choices, not for the fixed stabilizers nor the split ind.
        # nconfs = 4**3
        # 4 splitting choices for each of the 3 remaining splittings
        
        #we need a base configuration, from which we will change only
        #the splittings different from ind

        # first, we initialize all in 0
        baseconfiguration=[0]*len(self.stab)

        # now, we fix the values for our splitting choice, for which we need
        # to check the information correctly from splitting.getvalue()

        # order of the stabilizers on the syndrome
        stabOrder = (self.stab[2*splitInd].index, self.stab[2*splitInd+1].index)
        v0,v1 = self.splits[splitInd].getvalue(choice = choice,
                                               stabilizerOrder=stabOrder,
                                               cellGlobalIndex=self.i)

        baseconfiguration[2*splitInd]   = v0
        baseconfiguration[2*splitInd+1] = v1

        # now we fill the bulk stabilizer (nonsplits)
        nfixed = 4 # 4 bulk stabilizers on each cell in 488 code
        for i in range(nfixed):
            baseconfiguration[i-nfixed]=self.stab[i-nfixed].v


        # now, we need to loop over all 4^3 combinations of split choices
        # compute the syndrome corresponding to each of the choices
        # and compute the probability associated to the prior of each of the choices
        splitsToChange = [0,1,2,3]
        splitsToChange.pop(splitInd)
        splitDic = {}
        for i in range(3):
            splitDic[splitsToChange[i]] = i
        configurations = []
        probabilities  = []

        # loop over all choices
        for choice1 in range(4):
            for choice2 in range(4):
                for choice3 in range(4):
                    choices = [choice1,choice2,choice3]
                    configuration = list(baseconfiguration)
                    probability = 1.

                    # loop over all splittings (except the input splitting)
                    for thisSplit in splitsToChange:
                        stabOrder = (self.stab[2*thisSplit].index, self.stab[2*thisSplit+1].index)
                        thischoice = choices[splitDic[thisSplit]]
                        v0,v1 = self.splits[thisSplit].getvalue(choice =thischoice ,
                                                               stabilizerOrder=stabOrder,
                                                               cellGlobalIndex=self.i)

                        configuration[2*thisSplit]   = v0
                        configuration[2*thisSplit+1] = v1

                        probability *= self.fixprob(self.splits[thisSplit].psplit[thischoice])

                    configurations.append(configuration)
                    probabilities.append(self.fixprob(probability))

        return configurations, probabilities
    
    def splitconfGenerator(self):
        '''
        Generates all configurations of split values for the soft rescaling
        once we have the correction, and for each configuration computes:
         -the corresponding support of (the neighbors of) the changed splittings
         -the prior splitting probabilities.
        
        The output is a list qconfs with the support of the neighbors for the
        splitting configuration (for example, [0,0,1,1,1,0,0,0]),
        and the probability of each split configuration in another list
        pstabs.
        '''
        
        # we will only need to accound the configurations for the split
        # choices, not for the fixed stabilizers nor the split ind.
        # 4 possible choices times 4 splitting pairs
        nconfs = 4**4

        # The base stab configuration is the syndrome for the choice of splitting
        '''
        baseconfiguration=np.zeros(len(self.stab))

        # here we get the values of the stabilizers for the current choice
        for i in range(len(self.splits)):
            sorder = (self.stab[2*i].index, self.stab[2*i+1].index)
            v0,v1 = self.splits[i].getvalue(choice = -1,stabilizerOrder=sorder,
                                            cellGlobalIndex=self.i)
            # by choosing choice=-1, we select the current choice of the splitting
            baseconfiguration[2*i] = v0
            baseconfiguration[2*i+1] = v1

        # we add to the base configuration the values of the bulk stabilizers
        for bulk in [8,9,10,11]:
            baseconfiguration[bulk] = self.stab[bulk].v
        '''

        baseconfiguration = self.currentSyndrome()


        # qconfs refers to the qubits and whether they are excited by the
        # support of a stabilizer or not
        qconfs=[]
        #probability of this stab conf
        pconfs=[]

        # loop over all possible split choices
        for choice1 in range(4):
            for choice2 in range(4):
                for choice3 in range(4):
                    for choice4 in range(4):

                        choices = [choice1,choice2,choice3,choice4]
                        # split configuration:
                        configuration = np.zeros(8)
                        # support on the qubits of the difference of the split configuration
                        qconfiguration = np.zeros(18)

                        #probability of this splitting choice
                        probability = 1

                        # now we loop over all splittings to check the configuration and the probability
                        for thisSplit in range(4):
                            stabOrder = (self.stab[2 * thisSplit].index,
                                         self.stab[2 * thisSplit + 1].index)
                            thischoice = choices[thisSplit]
                            v0, v1 = self.splits[thisSplit].getvalue(choice=thischoice,
                                                                     stabilizerOrder=stabOrder,
                                                                     cellGlobalIndex=self.i)

                            configuration[2 * thisSplit] = v0
                            configuration[2 * thisSplit + 1] = v1

                            probability *= self.fixprob(self.splits[thisSplit].psplit[thischoice])

                        # now, to check the support, we need the difference in splittings between
                        # the reference configuration (the chosen one by the decoder)
                        # and this particular choice of splittings

                        difference = (baseconfiguration[:8] +configuration)%2
                        # now, we loop over the stabs in difference and add the support
                        for i in range(8):          #(loop over all stabs)
                            if difference[i]==1:    # add the support if that stab is different
                                stabIndex = self.stab[i].index
                                # we get the support of the NEIGHBOR of the stabilizer that has changed
                                support = self.splits[i//2].getSupport(stabGlobalIndex=stabIndex,
                                                                       cellGlobalIndex=self.i,
                                                                       neighbor=True)
                                for q in support:
                                    # q is the global index, we want to find the local index in the cell
                                    qin = self.qout[q]
                                    qconfiguration[qin] += 1
                        # finally, we store the support of all the stabilizers that have changed
                        qconfiguration %= 2
                        qconfs.append(qconfiguration)
                        # we can add to the list the probability of this choice of splittings
                        pconfs.append(self.fixprob(probability))


        # at this point, we have gone through all possible splitting choices
        # for each choice, we computed the difference in splitting choice with respect to
        # the reference splitting (the one with the highes probability, chosen in the split step)
        # and we computed the support of the NEIGHBOR of each of the stabilizers that has been changed
        # Additionally, we computed the probability of each of these split choices by multiplying
        # our estimate for the probability of each splitting choice

        return qconfs, pconfs
    
    def currentSyndrome(self):
        # returns the syndrome for the current split choice

        syndrome=np.zeros(len(self.stab))

        # here we get the values of the stabilizers for the current choice
        for i in range(len(self.splits)):
            sorder = (self.stab[2*i].index, self.stab[2*i+1].index)
            v0,v1 = self.splits[i].getvalue(choice = -1,stabilizerOrder=sorder,
                                            cellGlobalIndex=self.i)
            # by choosing choice=-1, we select the current choice of the splitting
            syndrome[2*i] = v0
            syndrome[2*i+1] = v1

        # we add to the base configuration the values of the bulk stabilizers
        for bulk in [8,9,10,11]:
            syndrome[bulk] = self.stab[bulk].v

        return syndrome

    
    
    

    def applylogicop(self,index):
        '''
        This function applies the logical operator on the
        logical qubit index (usually 0 or 1 for cells encoding 2 qubits)
        '''
        op=self.logicops[index]
        
        for q in op:
            qpair,ind,qmate=self.qd[q]
            self.qp[qpair].c[ind]=(self.qp[qpair].c[ind]+1)%2
        
        return
        
    def fixprob(self,p,tol=-20):
        '''
        This function fix probabilities under a certain threshold, so 
        that there is no problem with probabilities being zero
        '''

        if tol<0:
            tol = self.tol

        # if we dont know anything about the number due to a numerical error, we return maximum uncertainty p=0.5
        if p is np.nan:
            return tol

        newp=max(p,tol)#we fix a minimum value for p
        newp=min(newp,1-tol)#and a maximum value for p (never exactly one)
        return newp
        
        
    ################################### debug functions to study splittings

    def psingledebug(self, stabs, index):
        '''
        ### function without normalization step
        This function computes p(s_index|stabs)
        stabs: syndrome,  index: splitting index in the cell
        # computes the probability of a splitting choice, given
        # the rest of the splitting choices, by taking
        # p(e|syndrome)
        # and dividing it by the sum of all possible split choices
        # for the splitting "index", e.g. for index=0 :
        p(e|++,s2s3s4..)+p(e|+-,s2s3s4..)+p(e|-+,s2s3s4..)+p(e|--,s2s3s4..)

        # p(s0s1| s2s3s4...)  =     p(e|syndrome)
                    sum_choices ( p(e| s0's1'+syndrome) )
        '''
        pchoice = self.pall(stabs)

        pallChoices = 0.0
        choices = [(0, 0), (0, 1), (1, 0), (1, 1)]

        for choice in choices:
            changedstabs = list(stabs)
            changedstabs[2 * index] = (stabs[2 * index] + choice[0]) % 2
            changedstabs[2 * index + 1] = (stabs[2 * index + 1] + choice[1]) % 2

            pallChoices += self.pall(changedstabs)
        # we dont normalize
        pallChoices = self.fixprob(pallChoices)
        return self.fixprob(pchoice)

    def psoftdebug(self, split, choice):
        '''
        Computes p(s^u_0, s^u_1) by adding all configurations of split choices for the other splits

        :param split: split item for the splitting pair
        :param choice: integer for the current split choice
        '''

        # we get the index of the splitting
        splitIndex = self.splits.index(split)

        # we compute the different configurations of splittings
        configs, psplit = self.confGenerator(splitIndex, choice)

        totalp = 0.0
        for i in range(len(configs)):
            thisConf = configs[i]
            thisPSplit = psplit[i]
            newp = self.psingledebug(thisConf, splitIndex)

            # we multiply the conditional probability and the prior of the splitting probabilities
            # p(s0,s1 | S) * p(S)
            totalp += newp * thisPSplit

        return self.fixprob(totalp)
        
        