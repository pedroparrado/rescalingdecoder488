
"""
Color code decoder: qubit definition

Basic variables:

    q: list with the indices of the qubits in the pair (e.g. q= [3,4])

    p: matrix of probabilities of error:
        (e.g. p[0][1] is the probability of no error in the first qubit and error in the second)

    psingle: list of marginal error probabilities of each individual qubits, psingle =[p0,p1]

    e: list that determines if there is an error on each qubit,
        e.g. e=[0,1] means error in the second qubit

    c: same list but for the corrections, e.g. c=[1,1]  means correction on both qubits

    s: lists of stabilizers that contain each of the qubits, using the global index of the stabilizers
        e.g. s=[[0,1,2],[1,2,3]]

Functions:

    setpsingle(p0,p1) :
        useful to set the probabilities from the individual error probabilities (computes the matrix of p)

    setp( pmatrix):
        useful to set the probabilities from the matrix of probabilities (computes the marginals)

@author: pedroparrado
"""

class Qubitpair:
    
    '''
    This basic class contains information about the state of a qubit
    in the decoder: probability of error, an error happened on it,
    a correction was applied to it, and the indexes of the stabilizers
    that contain this qubit.
    Qubits are stored in pairs, as there might be some correlations in the
    error probabilities due to 
    '''
        
    def __init__(self,p):
        q=1.-p
        self.n=2#number of qubits
        #indices of the qubits in the pair
        self.q=[]
        
        #p contains the probability of error of the qubits
        #example: p[0][1] is the probability of error en the second qubit
        self.p=[[q*q,q*p],[p*q,p*p]]
        #####[["00","01"],["10","11"]]
        #psingle is each of the error probabilities
        self.psingle=[p,p]
        #e=0 means no error happened in the qubit
        self.e=[0,0]
        #c=0 means no correction is applied
        self.c=[0,0]
        #s is the list of stabilizers that contain the qubit. It contains
        #their index in the global decoder
        self.s=[[],[]]


    '''
    setup functions, to set the error probabilities either from the
    single qubit perspective or from the correlated 2qubit perspective
    '''
    def setpsingle(self,p0,p1):
        q0=1.-p0
        q1=1.-p1
        
        self.p=[[q0*q1,q0*p1],[p0*q1,p0*p1]]
        #####[["00","01"],["10","11"]]
        self.psingle=[p0,p1]
    
    def setp(self,p):
        assert len(p)==len(p[0]), "Quick check of dimensions of p failed"
        
        self.p=p
        self.psingle=[p[1][0]+p[1][1],p[0][1]+p[1][1]]
