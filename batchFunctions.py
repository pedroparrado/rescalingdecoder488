
import numpy as np
import matplotlib.pyplot as plt

from lattice488 import Code488
import time

import pickle
import os.path




'''
Data structure to run the simulations and store all parameters
'''
class Batch():
    def __init__(self ,name ,m=1 ,shortcuts=True ,cornerUpdates=True,
                 nsteps=0 ,nmean=4 ,BPsteps=3 ,hardSplits=False, hardRescale=False,
                 renormalizeOn = False, tolerance= 1e-15, fixp = -1,qubitTolerance = 1e-5,splitCutoff = -1,
                 useposterior = False):
        # contains all the parameters from a given simulation

        self.name = name
        self.m = m
        self.shortcuts = shortcuts                  # compute only the most probable configuration on the splits
        self.cornerUpdates = cornerUpdates          # update corner probabilities
        self.nsteps = nsteps                        # number of split update average rounds
        self.nmean = nmean                          # number of split update steps in between average rounds
        self.BPsteps = BPsteps                      # number of belief propagation steps
        self.hardSplits = hardSplits                # hard splits or not (not yet implemented in the code)
        self.hardRescale = hardRescale              # hard rescaling or not ( not yet implemented in the code
        self.renormalizeOn = renormalizeOn          # remapping of probabilities by doing cubic root
        self.tolerance = tolerance                  # tolerance of the fixprob() function
        self.fixp = fixp                            # fixing the initial qubit error probability
        self.qubitTolerance = qubitTolerance        # tolerance of fixprob() when applied to qubit probabilities
        self.splitCutoff = splitCutoff              # minimum value of the probability of a split choice
        self.useposterior = useposterior            # use posterior information on p(S) from the sum of all choices
        # dictionary that links probabilities with data points
        self.Pdata = {}

    def addData(self ,data):
        # includes data from a Data item data

        # if we dont have any data for this point
        if data.p not in self.Pdata:
            newp = data.p
            newData= Data(newp ,self.m)

            self.Pdata[data.p] = newData

        # combine the new data with the previous data
        self.Pdata[data.p].combine(data)

    def load(self ,filename = None, cutoffData = False):
        # loads data from previous runs and includes it in this batch class

        if filename is None:
            filename = self.name

        path = 'data/'
        if cutoffData:
            path = 'cdata/'

        if os.path.isfile(path +filename):
            with open(path + filename, 'rb') as file:
                newbatch = pickle.load(file)
            '''
            assert self.m == newbatch.m, "Different m parameters used"
            assert self.shortcuts == newbatch.shortcuts, "Different shortcuts parameters used"
            assert self.cornerUpdates == newbatch.cornerUpdates, "Different cornerUpdates parameters used"
            assert self.nsteps == newbatch.nsteps, "Different nsteps parameters used"
            assert self.nmean == newbatch.nmean, "Different nmean parameters used"
            assert self.BPsteps == newbatch.BPsteps, "Different BPsteps parameters used"
            assert self.hardSplits == newbatch.hardSplits, "Different hardSplits parameters used"
            assert self.hardRescale == newbatch.hardRescale, "Different hardRescale parameters used"
            '''

            # combine data from both batches
            for p in newbatch.Pdata:

                self.addData ( newbatch.Pdata[p])

        else:
            print ("File not exist")


    def save(self):

        with open(f'data/' + self.name, 'wb') as file:
            pickle.dump(self, file)
    def saveCutoff(self):
        with open(f'cdata/' + self.name, 'wb') as file:
            pickle.dump(self, file)



'''
Data structure for a single point in the threshold plot.
Includes information about the error probability, the size of the lattice, and:
    - number of samples
    - number of samples with an error in any of the 4 logical operators
    - array with the count of samples with an error on each of the logical ops
'''
class Data():
    def __init__(self, p, m, samples = 0 ,anyError = 0, eachError = 0):
        self.p = p
        self.m = m

        self.samples = samples
        self.anyError = anyError
        self.eachError  = eachError
        self.time = 0


    def combine(self ,otherData):

        #assert otherData.p == self.p, "this p " +str(self.p ) +" does not match  " +str(otherData.p)
        assert otherData.m == self.m, "this m " +str(self.m ) +" does not match  " +str(otherData.m)

        self.samples += otherData.samples
        self.anyError += otherData.anyError
        self.eachError += otherData.eachError
        self.time += otherData.time


'''
Function to run simulations with a batch
'''


def runSimulations(batch, parray, Nsamples):
    m = batch.m
    code = Code488(m, shortcuts=batch.shortcuts,tolerance=batch.tolerance,hardSplitting=batch.hardSplits,
                   qubitTolerance=batch.qubitTolerance, splitTolerance=batch.splitCutoff,
                   useposterior=batch.useposterior)

    for p in parray:
        nsteps = batch.nsteps
        nmean = batch.nmean
        BPsteps = batch.BPsteps
        cornerupdates = batch.cornerUpdates
        renormalizeOn = batch.renormalizeOn
        fixp = batch.fixp
        startTime = time.time()
        anyError, eachError = code.simulate(N=Nsamples, p=p,
                                            cornerupdates=cornerupdates, nsteps=nsteps, nmean=nmean,
                                            BPsteps=BPsteps, renormalizeOn=renormalizeOn, fixp=fixp)
        timeDif = time.time()-startTime
        newData = Data(p, m, samples=Nsamples, anyError=anyError, eachError=eachError)
        newData.time = timeDif
        batch.addData(newData)

        batch.save()


def runCutoffSimulations(batch, cutoffArray, Nsamples):
    m = batch.m

    code = Code488(m, shortcuts=batch.shortcuts,tolerance=batch.tolerance,hardSplitting=batch.hardSplits,
                   qubitTolerance=batch.qubitTolerance,splitTolerance=batch.splitCutoff,
                   useposterior=batch.useposterior)

    p = batch.fixp

    for iteration in range(Nsamples):

        code.clear(p)
        code.noise()
        code.syndrome()

        errorList = code.saveError()


        for cutoff in cutoffArray:

            nsteps = batch.nsteps
            nmean = batch.nmean
            BPsteps = batch.BPsteps
            cornerupdates = batch.cornerUpdates
            renormalizeOn = batch.renormalizeOn

            code.setCutoff(batch.tolerance, cutoff, batch.splitCutoff)

            code.clear(p)
            code.setError(errorList)

            code.decoder(cornerupdate=cornerupdates, nsteps=nsteps, nmean=nmean, niterBP=BPsteps,
                         renormalizeOn=renormalizeOn)
            anyError, eachError = code.check()

            newData = Data(cutoff, m, samples=1, anyError=anyError, eachError=eachError)

            batch.addData(newData)

        batch.saveCutoff()

def runCutoffSimulationsNoSave(batch, cutoffArray, Nsamples):
    m = batch.m
    code = Code488(m, shortcuts=batch.shortcuts,tolerance=batch.tolerance,hardSplitting=batch.hardSplits,
                   qubitTolerance=batch.qubitTolerance)

    p = batch.fixp

    for iteration in range(Nsamples):

        code.clear(p)
        code.noise()
        code.syndrome()

        errorList = code.saveError()


        for cutoff in cutoffArray:

            nsteps = batch.nsteps
            nmean = batch.nmean
            BPsteps = batch.BPsteps
            cornerupdates = batch.cornerUpdates
            renormalizeOn = batch.renormalizeOn

            code.setCutoff(cutoff)
            code.clear(p)
            code.setError(errorList)

            code.decoder(cornerupdate=cornerupdates, nsteps=nsteps, nmean=nmean, niterBP=BPsteps,
                         renormalizeOn=renormalizeOn)
            anyError, eachError = code.check()

            newData = Data(cutoff, m, samples=1, anyError=anyError, eachError=eachError)

            batch.addData(newData)

def formatTime(start):
    t = time.time() - start
    if t < 60:
        return str(round(t, 2)) + " s"
    elif t < 3600:
        min = t // 60
        s = t - min * 60
        return (str(int(min)) + " min " + str(int(s)) + " s")
    else:
        h = t // 3600
        min = (t - h * 3600) // 60
        s = t - min * 60 - h * 3600
        return (str(int(h)) + " h " + str(int(min)) + " min " + str(int(s)) + " s")