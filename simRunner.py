'''
Runs simulations to estimate the threshold of the decoder
'''


import numpy as np
import sys
from batchFunctions import Batch, runSimulations
import time


print("Running for lattice size: ", sys.argv)

m = int(sys.argv[1])
id = np.random.randint(100000)
np.random.seed(id)

name = "1thSetm"+str(m)+"id"+str(id)

batch = Batch(name=name,
              m=m,
              shortcuts=True,
              cornerUpdates=True,
              nsteps=0,
              nmean=7,
              BPsteps=3,
              hardRescale=False,
              hardSplits=False,
              renormalizeOn=False,
              tolerance=1e-25,
              qubitTolerance=1e-25,
              splitCutoff=1e-25,
              useposterior=False)
parray = np.linspace(0.055,0.08,8)
if m==4:
    parray = np.linspace(0.06,0.08,4)
if m==5:
    parray = np.linspace(0.065,.075,3)




def formatTime(start):
    t = time.time()-start
    if t<60:
        return str(round(t,2))+ " s"
    elif t<3600:
        min = t//60
        s = t-min*60
        return(str(int(min)) + " min "+str(int(s))+" s")
    else:
        h = t//3600
        min = (t-h*3600)//60
        s = t-min*60 - h*3600
        return(str(int(h)) + " h "+str(int(min)) + " min "+str(int(s))+" s")


# Iterations:
startTime = time.time()
iterPerBatch = [20,50,5,1,1,1,1,1]
for k in range(100000000):
    print("Iteration ",k)
    print("Time so far: ", formatTime(startTime))
    runSimulations(batch,parray,iterPerBatch[m])
