
import numpy as np
import matplotlib.pyplot as plt

from lattice488 import Code488
import time

import pickle
import os.path




'''
Data structure to run the simulations and store all parameters
'''
class Batch():
    def __init__(self ,name ,m=1 ,shortcuts=True ,cornerUpdates=True,
                 nsteps=0 ,nmean=4 ,BPsteps=3 ,hardSplits=False, hardRescale=False,
                 renormalizeOn = False, tolerance= 1e-15, fixp = -1, qubitTolerance =1e-15,
                 splitCutoff = -1, useposterior =False):
        # contains all the parameters from a given simulation

        self.name = name
        self.m = m
        self.shortcuts = shortcuts                  # compute only the most probable configuration on the splits
        self.cornerUpdates = cornerUpdates          # update corner probabilities
        self.nsteps = nsteps                        # number of split update average rounds
        self.nmean = nmean                          # number of split update steps in between average rounds
        self.BPsteps = BPsteps                      # number of belief propagation steps
        self.hardSplits = hardSplits                # hard splits or not (not yet implemented in the code)
        self.hardRescale = hardRescale              # hard rescaling or not ( not yet implemented in the code
        self.renormalizeOn = renormalizeOn          # remapping of probabilities by doing cubic root
        self.tolerance = tolerance                  # tolerance of the fixprob() function
        self.fixp = fixp                            # fixing the initial qubit error probability
        self.qubitTolerance = qubitTolerance        # numerical precision for the qubit probabilities
        self.splitCutoff = splitCutoff              # minimum value for a split probability
        self.useposterior = useposterior            # use posterior information on p(S) from the sum of all choices
        # dictionary that links probabilities with data points
        self.Pdata = {}

    def addData(self ,data):
        # includes data from a Data item data

        # if we dont have any data for this point
        if data.p not in self.Pdata:
            newp = data.p
            newData= Data(newp ,self.m)

            self.Pdata[data.p] = newData

        # combine the new data with the previous data
        self.Pdata[data.p].combine(data)

    def load(self ,filename = None):
        # loads data from previous runs and includes it in this batch class

        if filename is None:
            filename = self.name

        if os.path.isfile('bdata/' +filename):

            with open(f'bdata/' + filename, 'rb') as file:
                newbatch = pickle.load(file)

            assert self.m == newbatch.m, "Different m parameters used"
            assert self.shortcuts == newbatch.shortcuts, "Different shortcuts parameters used"
            assert self.cornerUpdates == newbatch.cornerUpdates, "Different cornerUpdates parameters used"
            assert self.nsteps == newbatch.nsteps, "Different nsteps parameters used"
            assert self.nmean == newbatch.nmean, "Different nmean parameters used"
            assert self.BPsteps == newbatch.BPsteps, "Different BPsteps parameters used"
            assert self.hardSplits == newbatch.hardSplits, "Different hardSplits parameters used"
            assert self.hardRescale == newbatch.hardRescale, "Different hardRescale parameters used"


            # combine data from both batches
            for p in newbatch.Pdata:

                self.addData ( newbatch.Pdata[p])

        else:
            print ("File not exist")


    def save(self):

        with open(f'bdata/' + self.name, 'wb') as file:
            pickle.dump(self, file)



'''
Data structure for a single point in the threshold plot.
Includes information about the error probability, the size of the lattice, and:
    - number of samples
    - number of samples with an error in any of the 4 logical operators
    - array with the count of samples with an error on each of the logical ops
    
    -benchmarking data for the splitings, separated in success and failure cases
'''
class Data():
    def __init__(self, p, m, samples = 0 ,anyError = 0, eachError = 0):
        self.p = p
        self.m = m

        self.samples = samples
        self.anyError = anyError
        self.eachError  = eachError

        self.splitSuccessData = {}
        self.splitFailureData = {}

        self.ecFailData = ECData(empty=True)
        self.ecSuccessData = ECData(empty=True)

        for i in range(m):
            self.splitSuccessData[m-i] = SplitData(m-i)
            self.splitFailureData[m-i] = SplitData(m-i)

    def combine(self ,otherData):

        assert otherData.p == self.p, "this p " +str(self.p ) +" does not match  " +str(otherData.p)
        assert otherData.m == self.m, "this m " +str(self.m ) +" does not match  " +str(otherData.m)

        self.samples += otherData.samples
        self.anyError += otherData.anyError
        self.eachError += otherData.eachError

        self.ecFailData.combine(otherData.ecFailData)
        self.ecSuccessData.combine(otherData.ecSuccessData)

        m=self.m
        for i in range(m):
            self.splitSuccessData[m-i].combine(otherData.splitSuccessData[m-i])
            self.splitFailureData[m-i].combine(otherData.splitFailureData[m-i])

class ECData():
    '''
    contains information about the number of errors and corrections, the difference and the histogram of that difference
    '''
    def __init__(self,e=0,c=0,empty=False):
        self.e=e
        self.c=c
        self.dif = e-c

        self.difhist = {}

        self.Mcorrections=[]

        if not empty:
            self.difhist[e-c]=1
    def addData(self,e,c):
        self.e += e
        self.c += c
        self.dif += e-c

        if e-c in self.difhist:
            self.difhist[e-c]= 1 + self.difhist[e-c]
        else:
            self.difhist[e-c] = 1
    def combine(self,otherData):
        self.e += otherData.e
        self.c += otherData.c
        self.dif += otherData.dif
        if len(self.Mcorrections)>0 and len(otherData.Mcorrections)>0:
            self.Mcorrections += otherData.Mcorrections
        elif len(otherData.Mcorrections)>0:
            self.Mcorrections = otherData.Mcorrections

        for ec in otherData.difhist:
            if ec in self.difhist:
                self.difhist[ec]= otherData.difhist[ec] + self.difhist[ec]
            else:
                self.difhist[ec] = otherData.difhist[ec]


class SplitData():
    '''
    Contains the data from the split benchmarking for a given point p,m,error
    Includes functions to combine datapoints
    '''
    def __init__(self,m):
        # first 3 parameters define the control variables: error probability, size of the lattice, and
        # if an error happened on that run or not  (we separate the data from successful runs and failure events)
        self.m = m

        # these are the data variables contained in the class
        # number of samples
        self.samples = 0
        # all the following variables are arrays representing the evolution of a magnitude with each split update step
        # energy of the system (sum of -log() of the probabilities of the current correction on each cell)
        self.energy = 0
        # energy of the system, normalized by the maximum energy of each run
        self.normalizedEnergy = 0
        # total change/split of the probabilities of splittings
        self.difference = 0
        # fraction of active splittings (more than 10% change in the probabilities)
        self.actives = 0
        # fraction of splits that changed their choice
        self.choiceDiff = 0
        # fraction of splits that changed their choice squared (for the variance)
        self.choiceDiff2 = 0
        # maximum probability for any of the split choices
        self.maxp = 0
        # minimum probability for any of the split choices (split with the max uncertainty)
        self.minp = 0
        # difference in the probability of the current split choice between split steps
        self.changepchoice = 0
        # maximum difference in the probabilities of any split group after one step
        self.maxchange = 0

        maxsteps = 40 # number of split steps to be considered #todo take this from the batch data
        minchange = 10 # number of splits that we allow to change to consider the trajectory converged
        # histogram of last step with less than n flips
        self.lastchangehist = np.zeros((minchange,maxsteps+1))

        # this data corresponds to the histogram of the probabilities of the chosen splits on the last update
        self.phist = np.linspace(0.2,1,101)
        self.histpchoice=np.zeros(100)




    def addData(self, energy=0,nEnergy=0,difference=0,actives=0,choiceDiff=0,maxp=0,minp=0,changepchoice=0,
                maxchange = 0, histpchoice = 0, samples=1, lastchangehist = 0, choiceDiff2=0):
        self.energy+=energy
        self.normalizedEnergy+=nEnergy
        self.difference+= difference
        self.actives+= actives
        self.choiceDiff+=choiceDiff
        self.choiceDiff2+=choiceDiff2
        self.maxp+= maxp
        self.minp+= minp
        self.changepchoice+=changepchoice
        self.histpchoice+=histpchoice
        self.samples+=samples
        self.maxchange+=maxchange
        self.lastchangehist+=lastchangehist

    def combine(self,otherData):
        if otherData.m == self.m:
            self.samples += otherData.samples
            self.energy+= otherData.energy
            self.normalizedEnergy+= otherData.normalizedEnergy
            self.difference+= otherData.difference
            self.actives+= otherData.actives
            self.choiceDiff+= otherData.choiceDiff
            self.choiceDiff2+= otherData.choiceDiff2
            self.maxp+= otherData.maxp
            self.minp+= otherData.minp
            self.changepchoice+= otherData.changepchoice
            self.maxchange+=otherData.maxchange

            self.histpchoice+=otherData.histpchoice
            self.lastchangehist+=otherData.lastchangehist

        else:
            print("Trying to merge Incompatible split data")
    def getData(self):
        # returns the data, after averaging over the samples
        # energy,nEnergy,difference,actives,changechoice,maxp,minp,changepchoice

        return self.energy/self.samples,self.normalizedEnergy/self.samples,self.difference/self.samples,\
               self.actives/self.samples,\
               self.choiceDiff/self.samples,self.maxp/self.samples,self.minp/self.samples,\
               self.changepchoice/self.samples, self.maxchange/self.samples, self.lastchangehist/self.samples,\
               self.choiceDiff2/self.samples

'''
Function to run simulations with a batch
'''
def registerChoice(code):
    n = len(code.splits)
    choices = []
    for split in code.splits:
        choices.append(split.choice)
    return np.array(choices)
def findpchoices(code):
    p=[]
    for split in code.splits:
        p.append(split.psplit[split.choice])
    return np.array(p)
# energy of a splitting configuration

def computeEnergy(code):
    '''
    For the current split choice, we find the energy as the sum of the logarithms of the probabilities of the
    most probable configurations in the cells compatible with the split choice
    '''
    code.updatesplitchoice()
    energy = 0
    for cell in code.cells:
        #find the probability of the most probable configuration given the current split choice

        syndrome = cell.currentSyndrome()
        configurations = cell.lookuptable(syndrome)
        pconf = cell.pconf(configurations[0])
        energy -= np.log(pconf)

    return energy

def lastRoundMaxChange(x,maxChange):
    try:
        return(np.max(np.where(x>maxChange)) )
    except:
        return(0)


def runSimulations(batch, parray, Nsamples):
    m = batch.m
    #batch.load()
    code = Code488(m, shortcuts=batch.shortcuts,tolerance=batch.tolerance,hardSplitting=batch.hardSplits,
                   qubitTolerance=batch.qubitTolerance, splitTolerance=batch.splitCutoff,
                   useposterior=batch.useposterior)

    nsteps = batch.nsteps
    nmean = batch.nmean
    BPsteps = batch.BPsteps
    cornerupdates = batch.cornerUpdates
    renormalizeOn = batch.renormalizeOn
    fixp = batch.fixp

    for p in parray:
        allData = Data(p,m)
        for iter in range(Nsamples):
            code.clear(p)
            code.noise()
            code.syndrome()
            bData = splitBenchmarkingDecoder(code,
                                            cornerupdate=cornerupdates, nsteps=nsteps, nmean=nmean,
                                            niterBP=BPsteps, renormalizeOn=renormalizeOn, fixp=fixp)
            anyError,eachError = code.check()
            errors,corrections = code.countErrors()
            Mcorrections = np.array(code.Ncorrection)

            newData = Data(p, m, samples=1, anyError=anyError, eachError=eachError)
            newECData = ECData(errors,corrections)
            newECData.Mcorrections = Mcorrections

            if anyError>0:
                # failure event
                newData.splitFailureData = bData
                newData.ecFailData = newECData
            else:
                newData.splitSuccessData = bData
                newData.ecSuccessData = newECData

            allData.combine(newData)

        batch.addData(newData)

        batch.save()


###############################################################
# DECODER
###############################################################


def splitBenchmarkingDecoder(code,cornerupdate=True, nsteps=1, nmean=5, niterBP=3, renormalizeOn=False, fixp=-5,
                             splitdatadict={}):
    '''
    Applies the decoder, and returns a dictionary with the splitBenchmarkingData for all relevant m sizes (m>0)
    '''

    # if we are fixing the probability of the qubits, we set it to the initial value given in the input
    if fixp > 0:
        for q in code.q:
            q.setpsingle(fixp, fixp)

    # first, we check if we are finished
    if code.m == 0:
        code.decoderm0()
        err, corr = code.countErrors()
        code.Ncorrection[code.m] = corr
        return splitdatadict

    # belief propagation prior update
    if niterBP > 0:
        code.beliefPropagation(niterBP)

    # second, update corners
    if cornerupdate:
        code.updatecorners()

    # splitting update
    code.splitinit()

    # code.splitupdates(nsteps, nmean)

    energy, normalizedEnergy, difference, actives, changechoice,changechoice2, maxp, minp, changepchoice, maxchange = splitUpdates(
                                                                                                          code,
                                                                                                          nsteps=nsteps,
                                                                                                          nmean=nmean)

    # recording the data from the split details in the dictionary
    splitData = SplitData( m=code.m )
    lastchangehist = np.zeros((10,41)) # todo make this dependent on the batch
    for maxch in range(10):
        #we find the last round at which >maxch splits changed their choice
        lastRound = lastRoundMaxChange(changechoice*len(code.splits),maxch)
        lastchangehist[maxch, lastRound] = 1

    splitData.addData(energy, normalizedEnergy, difference, actives, changechoice, maxp, minp, changepchoice,
                      maxchange = maxchange, lastchangehist=lastchangehist, choiceDiff2=changechoice2)
    # data for the histogram of the final pchoice
    pchoices = findpchoices(code)
    phist,_ = np.histogram(pchoices,splitData.phist) # counts = histogram( data, bins)
    splitData.histpchoice += phist
    # save the info
    splitdatadict[code.m] = splitData


    # find corrections and rescaling probability
    code.decodecells()
    code.rescaling()
    # decode the new code

    # counting the corrections at this stage
    err, corr = code.countErrors()
    code.Ncorrection[code.m] = corr


    # renormalization of qubit probabilities: map probabilities to higher probabilities
    if code.m > 1 and renormalizeOn:
        code.newcode.renormalizeProbabilities()

    # we dont call the standard decoder, but this decoder instead
    #code.newcode.decoder(cornerupdate, nsteps, nmean, niterBP, renormalizeOn, debugText)
    splitBenchmarkingDecoder(code.newcode, cornerupdate=cornerupdate, nsteps=nsteps, nmean=nmean,
                             niterBP=niterBP, renormalizeOn=renormalizeOn, fixp=-5, #make sure not to rewrite p's
                             splitdatadict=splitdatadict)


    code.backscale()
    # rescale back

    #register errors on lower levels
    for m in range(code.m):
        code.Ncorrection[m] = code.newcode.Ncorrection[m]

    return splitdatadict


'''
 We apply the soft splitting method to update the splitting probabilities.
The method can be extended to apply the mean value of last 2 steps.

nmean: number of updates between mean value steps
nsteps: number of cycles of nmean steps
'''


def splitUpdates(code, nsteps=0, nmean=10, geometricAverage=True):
    #    return energy,nEnergy,difference,actives,choiceDiff, choiceDiff2,maxp,minp,changepchoice, maxchange

    if nsteps == 0:
        return splitsNoMean(code, nmean)

    # variables to track
    energy = []
    distance = []
    pnew = np.zeros((len(code.splits), 4))
    difference = []
    actives = []
    choiceDiff = []
    choiceDiff2 = []
    maxchange = []
    energy.append(computeEnergy(code))
    prevchoice = registerChoice(code)

    maxp = []
    minp = []
    pchoices = findpchoices(code)
    maxp.append(np.max(pchoices))
    minp.append(np.min(pchoices))

    changepchoice = []
    prevpchoice = pchoices
    pprev = np.zeros((len(code.splits), 4))
    # end of variables to track
    for k in range(len(code.splits)):
        pprev[k] = code.updatesplit(code.splits[k])

    ###############
    # case nsteps>0
    ################
    for i in range(nsteps):
        # updates of the splittings
        for j in range(nmean):
            # first, we update the splittings with the new value
            diff = 0  # intialize difference between probabilities
            act = 0  # initialize active splits

            # compute new splits
            maxd = -1
            for k in range(len(code.splits)):
                dist = np.sum(np.abs((np.array(pprev[k]) - np.array(pnew[k]))))
                diff += dist
                if dist > .1:
                    act += 1
                if dist>maxd:
                    maxd = dist
                code.splits[k].psplit = pnew[k]
                pprev[k] = pnew[k]

            # update splits
            for k in range(len(code.splits)):
                pnew[k] = code.updatesplit(code.splits[k])

            # tracking variables
            code.updatesplitchoice()
            maxchange.append(maxd) # maximum change in the probabilities of any split group
            difference.append(diff * 1.0 / len(code.splits))  # difference in the probabilities
            actives.append(act * 1.0 / len(code.splits))  # active splits

            newchoice = registerChoice(code)
            choiced = np.abs(newchoice - prevchoice)
            for i in range(len(choiced)):
                choiced[i] = min(1, choiced[i])  # we cap the diference in choice by 1, otherwise choice_3-choice_1 =2
            choiceDiff.append(np.sum(choiced) * 1.0 / len(code.splits))
            choiceDiff2.append( (np.sum(choiced) * 1.0 / len(code.splits))**2)
            prevchoice = newchoice

            energy.append(computeEnergy(code))
            pchoices = findpchoices(code)
            changepchoice.append(np.sum(np.abs(pchoices - prevpchoice)) * 1.0 / len(code.splits))
            prevpchoice = pchoices

            maxp.append(np.max(pchoices))
            minp.append(np.min(pchoices))
            # end of tracking variables

        # meanvalue step every nmean iterations
        diff = 0
        act = 0
        maxd = -1
        for k in range(len(code.splits)):

            if geometricAverage:
                # geometric mean

                code.splits[k].psplit = np.sqrt(np.array(pnew[k]) * np.array(code.splits[k].psplit))
                # we need to renormalize the probabilities
                code.splits[k].psplit /= np.sum(code.splits[k].psplit)

                dist = np.sum(np.abs((np.array(code.splits[k].psplit) - np.array(pnew[k]))))
                diff += dist
                if dist > .1:
                    act += 1
                if dist > maxd:
                    maxd = dist
                pprev[k] = code.splits[k].psplit
            else:
                # arithmetic mean
                code.splits[k].psplit = (pnew[k] + code.splits[k].psplit) / 2.
                # we renormalize the probabilities, although shouldn't be necessary
                code.splits[k].psplit /= np.sum(code.splits[k].psplit)

                dist = np.sum(np.abs((np.array(code.splits[k].psplit) - np.array(pnew[k]))))
                diff += dist
                if dist > .1:
                    act += 1
                if dist>maxd:
                    maxd = dist
                pprev[k] = code.splits[k].psplit

        code.updatesplitchoice()
        maxchange.append(maxd)
        difference.append(diff * 1.0 / len(code.splits))
        actives.append(act * 1.0 / len(code.splits))

        newchoice = registerChoice(code)
        choiced = np.abs(newchoice - prevchoice)
        for i in range(len(choiced)):
            choiced[i] = min(1, choiced[i])  # we cap the diference in choice by 1, otherwise choice_3-choice_1 =2
        choiceDiff.append(np.sum(choiced) * 1.0 / len(code.splits))
        choiceDiff2.append( (np.sum(choiced) * 1.0 / len(code.splits) )**2 )
        prevchoice = newchoice

        energy.append(computeEnergy(code))
        pchoices = findpchoices(code)
        changepchoice.append(np.sum(np.abs(pchoices - prevpchoice)) * 1.0 / len(code.splits))
        prevpchoice = pchoices

        maxp.append(np.max(pchoices))
        minp.append(np.min(pchoices))

    # return energy,distance
    #region turning into array

    energy = np.array(energy)
    maxchange = np.array(maxchange)
    difference, actives, choiceDiff = np.array(difference), np.array(actives), np.array(choiceDiff)
    choiceDiff2 = np.array(choiceDiff2)
    maxp, minp, changepchoice = np.array(maxp), np.array(minp), np.array(changepchoice)


    nEnergy = energy / np.max(energy)
    return energy, nEnergy, difference, actives, choiceDiff, choiceDiff2, maxp, minp, changepchoice, maxchange


def splitsNoMean(code, nsteps=10):
    pnew = np.zeros((len(code.splits), 4))
    energy = np.zeros(nsteps + 1)
    difference = []
    actives = []
    choiceDiff = []
    choiceDiff2 = []
    maxchange = []
    energy[0] = computeEnergy(code)
    prevchoice = registerChoice(code)

    maxp = []
    minp = []
    pchoices = findpchoices(code)
    maxp.append(np.max(pchoices))
    minp.append(np.min(pchoices))

    changepchoice = []
    prevpchoice = pchoices
    pprev = np.zeros((len(code.splits), 4))
    for j in range(nsteps):

        diff = 0.
        act = 0.
        maxd = -1
        for k in range(len(code.splits)):
            pnew[k] = code.updatesplit(code.splits[k])

        for k in range(len(code.splits)):
            # dist = np.sum(np.abs((np.array(code.splits[k].psplit)  - np.array(pnew[k]))))
            dist = np.sum(np.abs((np.array(pprev[k]) - np.array(pnew[k]))))

            diff += dist
            if dist > 0.1:
                act += 1
            if dist>maxd:
                maxd = dist

        # no meanvalue step
        for k in range(len(code.splits)):
            code.splits[k].psplit = pnew[k]
            pprev[k] = pnew[k]
        code.updatesplitchoice()

        maxchange.append(maxd)
        difference.append(diff * 1.0 / len(code.splits))
        actives.append(act * 1.0 / len(code.splits))

        newchoice = registerChoice(code)
        choiced = np.abs(newchoice - prevchoice)
        for i in range(len(choiced)):
            choiced[i] = min(1, choiced[i])  # we cap the diference in choice by 1, otherwise choice_3-choice_1 =2
        choiceDiff.append(np.sum(choiced) * 1.0 / len(code.splits))
        choiceDiff2.append( (np.sum(choiced) * 1.0 / len(code.splits) )**2 )
        prevchoice = newchoice

        energy[j + 1] = computeEnergy(code)
        pchoices = findpchoices(code)
        changepchoice.append(np.sum(np.abs(pchoices - prevpchoice)) * 1.0 / len(code.splits))
        prevpchoice = pchoices

        maxp.append(np.max(pchoices))
        minp.append(np.min(pchoices))
    #turning everything into arrays
    energy = np.array(energy)
    maxchange = np.array(maxchange)
    difference, actives, choiceDiff = np.array(difference), np.array(actives), np.array(choiceDiff)
    choiceDiff2 = np.array(choiceDiff2)
    maxp, minp, changepchoice = np.array(maxp), np.array(minp), np.array(changepchoice)
    nEnergy = energy / np.max(energy)
    return energy, nEnergy, difference, actives, choiceDiff,choiceDiff2, maxp, minp, changepchoice, maxchange


### function for the testing of soft and hard splits

def compareSplitEnergies(m=2,plotEnergies= True,p=0.09):
    # first iteration for soft splits
    shortcuts = True
    tolerance = 1e-55
    cornerupdate = True
    nsteps = 0
    nmean = 10
    BPsteps = 3
    renormalizeOn = False
    fixp = 0.01
    hardsplits = False

    niterBP = BPsteps
    code = Code488(m, p=p, shortcuts=shortcuts, tolerance=tolerance, hardSplitting=hardsplits)
    code.noise()
    code.syndrome()

    if fixp > 0:
        for q in code.q:
            q.setpsingle(fixp, fixp)
    code.beliefPropagation(niterBP)

    # second, update corners
    if cornerupdate:
        code.updatecorners()

    code.splitinit()
    energy, normalizedEnergy, difference, actives, changechoice,changechoice2, maxp, minp, changepchoice, maxpchange= splitUpdates(code, nsteps,
                                                                                                          nmean)
    energySoft = energy

    ### save error
    errorlist = []
    for qi in range(len(code.q)):
        q = code.q[qi]
        if q.e[0] == 1:
            errorlist.append(2 * qi)
        if q.e[1] == 1:
            errorlist.append(2 * qi + 1)

    # second iteration for hard splits
    # parameters

    shortcuts = True
    tolerance = 1e-55
    cornerupdate = True
    nsteps = 2
    nmean = 30
    BPsteps = 3
    renormalizeOn = False
    fixp = 0.01
    hardsplits = True

    niterBP = BPsteps

    code = Code488(m, p=p, shortcuts=shortcuts, tolerance=tolerance, hardSplitting=hardsplits)
    code.setError(errorlist)

    if fixp > 0:
        for q in code.q:
            q.setpsingle(fixp, fixp)

    code.beliefPropagation(niterBP)

    # second, update corners
    if cornerupdate:
        code.updatecorners()

    code.splitinit()
    energy, normalizedEnergy, difference, actives, changechoice,changechoice2, maxp, minp, changepchoice = splitUpdates(code,
                                                                                                          nsteps,
                                                                                                          nmean)

    # plot all
    if plotEnergies:
        plt.figure(figsize=(5, 3))
        plt.plot(energy, label="Hard splitting")
        plt.plot(energySoft, label="Soft splitting")
        plt.title("Energy")
        plt.xlabel("split steps")
        plt.ylabel("Energy")
        plt.yscale("log")
        plt.legend()

    return energySoft, energy

class EnergyData():
    def __init__(self,filename,filepath="bdata/energyData/",p=0.09,
                 m=2,energyhard=0,energysoft=0, normalizedhard=0,normalizedsoft=0, niter=0):
        self.m = m
        self.p = p
        self.filename = filename
        self.filepath = filepath+filename
        self.energyhard = energyhard
        self.energysoft = energysoft
        self.normalizedhard = normalizedhard
        self.normalizedsoft = normalizedsoft
        self.niter = niter

    def addValues(self,energyhard,energysoft,normalizedhard,normalizedsoft,niter):
        # adds individual set of values to the data
        self.energyhard += energyhard
        self.energysoft += energysoft
        self.normalizedhard += normalizedhard
        self.normalizedsoft += normalizedsoft
        self.niter += niter

    def combine(self,otherData):
        # combines two EnergyData classes
        assert otherData.m == self.m , "Different m values: "+str(self.m)+" vs "+str(otherData.m)
        self.energyhard += otherData.energyhard
        self.energysoft += otherData.energysoft
        self.normalizedhard += otherData.normalizedhard
        self.normalizedsoft += otherData.normalizedsoft
        self.niter += otherData.niter

    def loadData(self,filename = None):

        if filename is None:
            filename = self.filepath

        if os.path.isfile('data/' +filename):

            with open(filename, 'rb') as file:
                newdata = pickle.load(file)
            # combine data from both batches
            self.combine(newdata)

        else:
            print ("File not exist")
    def saveData(self):

        with open(self.filepath, 'wb') as file:
            pickle.dump(self, file)

    def plotEnergy(self,figsize = (7,3),normalized = False):
        # plots the energy data
        if normalized:
            energy,energySoft = self.normalizedhard, self.normalizedsoft
        else:
            energy,energySoft = self.energyhard, self.energysoft
        plt.figure(figsize=figsize)
        plt.plot(energy/self.niter, label="Hard splitting")
        plt.plot(energySoft/self.niter, label="Soft splitting")
        plt.title("Energy")
        plt.xlabel("split steps")
        plt.ylabel("Energy")
        plt.yscale("log")
        plt.legend()


    def plotAll(self,figsize = (15,3)):
        # plots all energy data
        niters = self.niter
        energies = self.energyhard
        softenergies = self.energysoft
        neh = self.normalizedhard
        nes = self.normalizedsoft
        plt.figure(figsize=figsize)
        plt.subplot(121)
        plt.plot(energies / niters, label="Hard splitting")
        plt.plot(softenergies / niters, label="Soft splitting")
        plt.title("Average Energy m=" + str(self.m))
        plt.xlabel("split steps")
        plt.ylabel("Energy")
        plt.yscale("log")
        plt.legend()
        plt.subplot(122)
        plt.plot(neh / niters, label="Hard splitting")
        plt.plot(nes / niters, label="Soft splitting")
        plt.title("Average Normalized Energy ")
        plt.xlabel("split steps")
        plt.ylabel("Energy")
        plt.yscale("log")
        plt.legend()
        print(niters, " Iterations ")


def oneRunChange(batch, p, plotChange=True,figsize = (15,7)):
    m = batch.m
    code = Code488(m, shortcuts=batch.shortcuts,tolerance=batch.tolerance,hardSplitting=batch.hardSplits,
                   qubitTolerance=batch.qubitTolerance, splitTolerance=batch.splitCutoff,
                   useposterior=batch.useposterior)

    nsteps = batch.nsteps
    nmean = batch.nmean
    BPsteps = batch.BPsteps
    cornerupdates = batch.cornerUpdates
    renormalizeOn = batch.renormalizeOn
    fixp = batch.fixp

    allData = Data(p,m)
    for iter in range(1):
        code.clear(p)
        code.noise()
        code.syndrome()
        bData = splitBenchmarkingDecoder(code,
                                        cornerupdate=cornerupdates, nsteps=nsteps, nmean=nmean,
                                        niterBP=BPsteps, renormalizeOn=renormalizeOn, fixp=fixp)

        anyError,eachError = code.check()
        errors,corrections = code.countErrors()
        Mcorrections = np.array(code.Ncorrection)

        newData = Data(p, m, samples=1, anyError=anyError, eachError=eachError)
        newECData = ECData(errors,corrections)
        newECData.Mcorrections = Mcorrections

        if anyError>0:
            # failure event
            newData.splitFailureData = bData
            newData.ecFailData = newECData
        else:
            newData.splitSuccessData = bData
            newData.ecSuccessData = newECData

        allData.combine(newData)

    batch.addData(newData)

    if plotChange:
        plt.figure(figsize = figsize)
        plt.subplot(131)
        plt.plot(bData[m].difference)
        plt.title("Difference in p")
        plt.xlabel("Split step")
        plt.ylabel("Change/split")
        plt.yscale("log")
        plt.subplot(132)
        plt.plot(bData[m].maxchange)
        plt.title("Maximum change in p")
        plt.xlabel("Split step")
        plt.yscale("log")
        plt.subplot(133)
        errors = np.sqrt(  (bData[m].choiceDiff2 - bData[m].choiceDiff **2  )*1.0/bData[m].samples)
        print(errors)
        #plt.plot(bData[m].choiceDiff)
        x = range(len(errors))
        y = bData[m].choiceDiff

        plt.errorbar(x,y,yerr=errors,linestyle="",marker ='o')
        plt.title("Number of changes in the split choice")
        plt.xlabel("Split step")
        plt.ylabel("Flips/split")

    return bData[m].difference, bData[m].maxchange, bData[m].choiceDiff, bData[m].choiceDiff2

